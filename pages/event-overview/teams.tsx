import PageLayout from "../../src/modules/layout/components/PageLayout";
import TeamsOverview from "../../src/modules/team/components/TeamsOverview";
import ContentPadding from "../../src/modules/layout/components/ContentPadding";

import BackLink from "../../src/modules/core/components/BackLink";
import ArrowLeft from "../../src/modules/icons/components/ArrowLeft";

export default () => {
  return (
    <PageLayout>
      <ContentPadding>
        <TeamsOverview/>
        <BackLink href="/" icon={<ArrowLeft />}>
          Back to home page
        </BackLink>
      </ContentPadding>
    </PageLayout>
  );
};
