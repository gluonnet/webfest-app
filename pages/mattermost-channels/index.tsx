import PageLayout from "../../src/modules/layout/components/PageLayout";
import MattermostChannelList from "../../src/modules/mattermost/components/MattermostChannelList";
import ContentPadding from "../../src/modules/layout/components/ContentPadding";

import BackLink from "../../src/modules/core/components/BackLink";
import ArrowLeft from "../../src/modules/icons/components/ArrowLeft";

export default () => {
  return (
    <PageLayout>
      <ContentPadding>
        <MattermostChannelList />
        <BackLink href="/" icon={<ArrowLeft />}>
          Back to home page
        </BackLink>
      </ContentPadding>
    </PageLayout>
  );
};
