import React from "react";
import PageLayout from "../../src/modules/layout/components/PageLayout";
import ChallengeList from "../../src/modules/challenge/components/ChallengeList";
import { ChallengeSelected } from "../../src/modules/user/components/ChallengeSelection";

import BackLink from "../../src/modules/core/components/BackLink";
import ArrowLeft from "../../src/modules/icons/components/ArrowLeft";

export default () => (
  <PageLayout>
    <ChallengeSelected />
    <ChallengeList />
    <BackLink href="/" icon={<ArrowLeft />}>
      Back to home page
    </BackLink>
  </PageLayout>
);
