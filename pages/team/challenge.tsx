import React from "react";
import Challenge from "../../src/modules/team/components/Challenge";
import PageLayout from "../../src/modules/layout/components/PageLayout";

import BackLink from "../../src/modules/core/components/BackLink";
import ArrowLeft from "../../src/modules/icons/components/ArrowLeft";

export default () => (
  <PageLayout>
    <Challenge />
    <BackLink href="/" icon={<ArrowLeft />}>
      Back to home page
    </BackLink>
  </PageLayout>
);
