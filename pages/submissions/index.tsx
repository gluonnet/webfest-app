import React from "react";
import PublicPageLayout from "../../src/modules/layout/components/PublicPageLayout";
import SubmittedProjectList from "../../src/modules/submittedProject/components/SubmittedProjectList";

export default () => {
  return (
    <PublicPageLayout title="Explore projects from the CERN Webfest 2022">
      <SubmittedProjectList />
    </PublicPageLayout>
  );
};
