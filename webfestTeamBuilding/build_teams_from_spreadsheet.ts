import { isTSPropertySignature } from "@babel/types";
import { clearLine } from "readline";
import project from "../pages/team/project";
import prisma from "../prisma/prisma";
import { getDoc, getBlankDoc } from "./spreadsheet";

const theTeamsGSID = "1CLKNJRrV2hx8WGJJFBKI0M5EhFJlCqeL38IweFSPIiY";

function getSheetByTitle(doc, sheetTitle) {
    for (let sheet of doc.sheetsByIndex) {
        if( sheetTitle == sheet.title ) {
            return sheet;
        }
    }
    return undefined;
}


async function getTeamInfo(theTeamsGSID) {
  const teamsDoc = await getDoc(theTeamsGSID);
  console.log(teamsDoc.title);

  let shTeamInfo = getSheetByTitle(teamsDoc, "WebFestTeams-FINAL");
  const rows = await shTeamInfo.getRows({ offset: 0, limit: 100000 });

  let teams = { };
  const teamId = (teamIndex: number) =>
    "team-" + String(teamIndex).padStart(2, '0');

  for (let row of rows) {
    if (row["team"] in teams) {
      teams[row["team"]]["members"].push( { id: row["email"] } );
    } else {
      var teamChallenge = await prisma.challenge.findOne({
        where: {
          id: row["challengeid"],
        }
      });
      teams[row["team"]] = {
        id: teamId(row["team"]+1),
	chId: row["challengeid"],
	topic: teamChallenge["primaryTopicId"],
	members: [ { id: row["email"] } ]
      }
    }
  }

  //console.log(teams)
  return teams;
}

export default async () => {
  let teams = await getTeamInfo(theTeamsGSID);
  //console.log(teams)

  let team_cnt = 0;
  for (let tId in teams) {
    let t = teams[tId];
    console.log(`Creating ${t.id} with ${t.members.length} members`);
    console.log(`==> Challenge: ${t.chId}; Topic: ${t.topic}`);
    t.members.forEach( (u) =>  ( console.log(`==> ++ ${u.id}`) ) );
    continue
    await prisma.team.upsert({
      where: { id: t.id },
      create: {
        id: t.id,
        primaryTopic: {
          connect: { id: t.topic}
        },
        challengeSelected: {
          connect: { id: t.chId}
        },
        members: {
          connect: t.members.map((u) => ({
            id: u.id,
          })),
        }
      },
      update: {},
    });
  }
}
