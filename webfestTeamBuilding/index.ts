import e from "express";
import team from "../pages/team";
import prisma from "../prisma/prisma";
import MemberListItem from "../src/modules/team/components/MemberListItem";
import { ChallengeSelected } from "../src/modules/user/components/ChallengeSelection";

const TEAM_ID_STARTS_AT = 9;
const teamId = (teamIndex: number) =>
  //"team-" + (TEAM_ID_STARTS_AT + teamIndex);
  "team-" + String(TEAM_ID_STARTS_AT + teamIndex).padStart(2, '0');

const teamMembers = {
  "team-01": [ "btech60115.20@bitmesra.ac.in", "pratyashakumar@gmail.com", "raviravithedon23@gmail.com", "abhinav6722cool@gmail.com" ],
  "team-02": [ "gian.luigi.d'alessandro@cern.ch", "adam.abed.abud@cern.ch", "fabian.metzger@cern.ch", "elisabetta.giulia.parozzi@cern.ch", "natalia.triantafyllou@cern.ch" ],
  "team-03": [ "jainanorudh75@gmail.com", "shubhamkjain@outlook.com", "gneha21@yahoo.in", "gmmgupta@gmail.com" ],
  //"team-03": [ "jainanorudh75@gmail.com", "gneha21@yahoo.in" ],
  "team-04": [ "gokul10012000@gmail.com", "syedabdulhannan00@gmail.com", "tkevinbiju@hotmail.com" ],
  "team-05": [ "banerjeerosy4@gmail.com", "banerjeenisha.1794@gmail.com", "anushbhatia1234@gmail.com", "eparamasari@gmail.com", "pgangapurwala@gmail.com", "anushka11b3@gmail.com", "neethu2001nsr@gmail.com", "peirisasel373@gmail.com" ],
  "team-06": [ "damla.gozuk@cern.ch", "hassan.ahmed@cern.ch", "eisha.mazhar19@gmail.com", "mahnoorimtiaz2710@gmail.com", "ozcsel.16@gmail.com", "maria.camila.diaz.sanchez@cern.ch", "aroosha.pervaiz@cern.ch", "augustin.tribolet@cern.ch", "tigran.ramazyan@cern.ch" ],
  //"team-06": [ "damla.gozuk@cern.ch", "eisha.mazhar19@gmail.com", "mahnoorimtiaz2710@gmail.com", "ozcsel.16@gmail.com", "aroosha.pervaiz@cern.ch", "augustin.tribolet@cern.ch", "tigran.ramazyan@cern.ch" ],
  "team-07": [ "jean.yves.beaucamp@cern.ch", "jan.willeke@cern.ch", "aditya.hilmy@cern.ch", "irene.andreou@cern.ch", "olivia.reinicke@cern.ch", "nadia.mariana.leal.reyes@cern.ch", "jan.herdieckerhoff@cern.ch", "lucas.van.mol@cern.ch", "janna.zoe.vischer@cern.ch" ],
  //"team-07": [ "jean.yves.beaucamp@cern.ch", "jan.willeke@cern.ch", "aditya.hilmy@cern.ch", "irene.andreou@cern.ch", "olivia.reinicke@cern.ch", "nadia.mariana.leal.reyes@cern.ch", "jan.herdieckerhoff@cern.ch", "janna.zoe.vischer@cern.ch" ],
  "team-08": [ "jansturan@gmail.com", "ilgaz.kaya@metu.edu.tr", "zeynepvarolxx@gmail.com", "yazici.furkan@protonmail.com", "sarp.algan@metu.edu.tr" ],
};

export default async () => {

  /*
  // Bug fixing
  let theUsers = await prisma.user.findMany({
    where: {
      //preferredChallenge: {some: {id: {equals: "5ad57560-f819-49c8-9a00-5e215eeb4b04"}}},
      preferredChallengeId: "5ad57560-f819-49c8-9a00-5e215eeb4b04"
    },
  });

  for (let u of theUsers) {
    console.log(u.email);
  }

  return;
  */

  // Making pre-selected teams
  for (let i = 8 ; i < 9 ; i++) {
    continue;
    let theTeamId = "team-" + String(i).padStart(2, '0');
    console.log(`Creating pre-formed team ${theTeamId}...`);
    await prisma.team.upsert({
      where: { id: theTeamId },
      create: { id: theTeamId },
      update: {},
    })
    if (teamMembers[theTeamId] === undefined) continue;
    for (let uEmail of teamMembers[theTeamId]) {
      console.log(uEmail);
      await prisma.user.update({
        where: { email : uEmail },
        data: { team: { connect: { id: theTeamId } } },
      })
    }

    // TODO: assign proper challenge to team
  }
/*
  const organiserUsers = await prisma.user.findMany({
    where: {
      roles: {some: {id: {equals: "admin"}}}
    },
  });

  for (let u of organiserUsers) {
    console.log(u)
  }
*/

  const usersWithChallengeWithoutTeam = await prisma.user.findMany({
    where: {
      team: null,
      preferredChallengeId: { not: null },
    },
  });

  var teams = {};
  let teamIdx = 0;

  for (let u of usersWithChallengeWithoutTeam) {
    if (u.teamId != null) continue;
    //console.log(u);
    if (teams[u.preferredChallengeId] === undefined) {
      teams[u.preferredChallengeId] = { id: teamId(teamIdx++), members: [ { id: u.id } ], challengeId: u.preferredChallengeId };
    } else {
      teams[u.preferredChallengeId]["members"].push({ id: u.id });
    }
  }

  for (let chId in teams) {

    var t = teams[chId];
    var teamChallenge = await prisma.challenge.findOne({
      where: {
        id: chId,
      }
    });

    teams[chId]["topic"] = teamChallenge["primaryTopicId"];
    teams[chId]["challenge"] = teamChallenge["title"];
    console.log(t);
  }

  let team_cnt = 0;
  for (let chId in teams) {
    var t = teams[chId];
    if ( t.members.length < 0 ) {
      console.log(`Skipping team because there are not enough members (${t.members.length}): ${t.challenge}`);
      console.log(`Would-be members:`);
      for (let u of t.members ) {
        let theUser = await prisma.user.findOne({where: {id: u.id}});
        console.log(`\t${theUser.email}`); 
      }
      continue;
    }
    let finalTeamId = teamId(team_cnt++);
    // Overriding team ID
    t.id = finalTeamId;
    console.log(`Creating team ${t.id} with ${t.members.length} members workign on ${t.challenge}.`);
    continue;
    await prisma.team.upsert({
      where: { id: t.id },
      create: {
        id: t.id,
        primaryTopic: {
          connect: { id: t.topic}
        },
        challengeSelected: {
          connect: { id: chId}
        },
        members: {
          connect: t.members.map((u) => ({
            id: u.id,
          })),
        }
      },
      update: {},
    });
    //break;

  }

  const usersWithoutChallengeWithoutTeam = await prisma.user.findMany({
    where: {
      team: null,
      preferredChallengeId: null,
    },
  });

  console.log(`There are ${usersWithoutChallengeWithoutTeam.length} without a team.`);

};
