
# CERN Webfest

This README describes how to connect to the CERN instance of the Webfest app @ https://app.webfest.cern.

## Running locally

To run locally, you need to proxy the database locally, and run the image from the GitLab Registry.

```
ssh -N -f -L 6603:itrac5439:6603 lxplus.cern.ch
```

Note that the instance might change servers. Ask Karolos for the right instance.

```
docker login gitlab-registry.cern.ch -u karolos
docker run -d -p 8080:8080/tcp --name webfest-app --rm --env-file docker.env.list gitlab-registry.cern.ch/gluonnet/webfest-app:latest
```

The `docker.env.list` file isn't committed for security reasons. Please reach out to Karolos if you need it.

At this point, the app is available on `http://localhost:8080`.


You can also use the docker container to run migrations and other database operations.


## Clearing the database and updating the list of participants

The following assumes that the list of participants is stored in a CSV file at `seed/registrations.csv` (it may be a symlink) with the followig headers

```
ID,Name,First Name,Last Name,Email Address,Phone Number,Title,Registration date,Registration state
```

(note that you will need to adjust the Indico registration export to achieve that goal).

Ensure that the `seed/index.ts` enables the items `cleanup` and `users_from_indico`, and copy it into the container using


```
docker cp seed/index.ts webfest-app:/app/seed/
```

Then execute it using

```
docker exec -it webfest-app bash
```

Then run

```
yarn seed
```

which will run the necessary database operations. You may need to edit `seed/cleanup.ts` and use `docker cp` to bring it into the running container.

Once done don't forget to push the changes, which will then also update the image in the GitLab Registry (created automatically by the CI).
