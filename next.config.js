module.exports = {
  publicRuntimeConfig: {
    HAS_SLACK: process.env.HAS_SLACK,
    SLACK_SIGNUP_URL: process.env.SLACK_SIGNUP_URL,
    HAS_MATTERMOST: process.env.HAS_MATTERMOST,
    MATTERMOST_SIGNUP_URL: process.env.MATTERMOST_SIGNUP_URL,
    MATTERMOST_TEAM_URL: process.env.MATTERMOST_TEAM_URL,
    HAS_FEEDBACK: process.env.HAS_FEEDBACK,
    FEEDBACK_URL: process.env.FEEDBACK_URL,
    HAS_SIGNUP: process.env.HAS_SIGNUP,
    CHALLENGE_SELECTION_UNTIL: process.env.CHALLENGE_SELECTION_UNTIL,
  },
  experimental: {
    async redirects() {
      // 307 temporary redirect
      const redirects = [
        {
          source: "/__health",
          destination: "/api/__health",
          permanent: true,
        },
        {
          source: "/challengeselection",
          destination: "/team/challenge",
          permanent: true,
        },
      ];

      if (process.env.HAS_SIGNUP == false) {
        redirects.push({
          source: "/signup(.*)",
          destination: "/",
          permanent: true,
        });
      }

      if (process.env.HAS_FEEDBACK) {
        redirects.push({
          source: "/feedback",
          destination: process.env.FEEDBACK_URL,
          permanent: true,
        });
      }

      if (process.env.HAS_SLACK) {
        redirects.push({
          source: "/slack",
          destination: process.env.SLACK_SIGNUP_URL,
          permanent: true,
        });
      }

      if (process.env.HAS_MATTERMOST) {
        redirects.push({
          source: "/mattermost",
          destination: process.env.MATTERMOST_SIGNUP_URL,
          permanent: true,
        });
      }

      return redirects;
    },
  },
};
