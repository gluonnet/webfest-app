import gql from "graphql-tag";

export const TeamFragment = gql`
  fragment Team on Team {
      id
      projects {
        id
        title
        videoUrl
      }
      slack {
        id
        url
      }
      challengeSelected {
        id
        title
      }
      members {
        id
      }
  }
`;
