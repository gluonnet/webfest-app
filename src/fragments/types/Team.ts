/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: Team
// ====================================================

export interface Team_projects {
  __typename: "Project";
  id: string;
  title: string;
  videoUrl: string | null;
}

export interface Team_slack {
  __typename: "SlackConversation";
  id: string;
  url: string;
}

export interface Team_challengeSelected {
  __typename: "Challenge";
  id: string;
  title: string;
}

export interface Team_members {
  __typename: "User";
  id: string;
}

export interface Team {
  __typename: "Team";
  id: string;
  projects: Team_projects[];
  slack: Team_slack | null;
  challengeSelected: Team_challengeSelected | null;
  members: Team_members[];
}
