import Link from "next/link";
import React, { CSSProperties, useRef, useState } from "react";
import styled from "styled-components";
import SimpleSchema from "simpl-schema";
import SimpleSchema2Bridge from "uniforms-bridge-simple-schema-2";
import { AutoForm } from "uniforms-material";
import { LinkButton } from "../../core/components/Button";
import ContentPadding from "../../layout/components/ContentPadding";
import Spacer from "../../layout/components/Spacer";
import Schedule from "../../schedule/components/Schedule";
import WelcomeMessage from "../../user/components/WelcomeMessage";
import useMe from "../../user/hooks/useMe";
import useSendLoginEmail from "../../user/hooks/useSendLoginEmail";
import InternalBigLink from "../../core/components/InternalBigLink";
import Heading from "../../layout/components/Heading";
import useMyTeam from "../../user/hooks/useMyTeam";
import getConfig from "next/config";
import BigLink from "../../core/components/BigLink";
import ArrowRight from "../../icons/components/ArrowRight";

import { ChallengeSelect } from "../../user/components/ChallengeSelection";

const { publicRuntimeConfig } = getConfig();
const hasSignup = publicRuntimeConfig?.HAS_SIGNUP == "true";
const hasSlack = publicRuntimeConfig?.HAS_SLACK == "true";
const hasMattermost = publicRuntimeConfig?.HAS_MATTERMOST== "true";
const hasFeedback = publicRuntimeConfig?.HAS_FEEDBACK == "true";

export interface HomeProps {
  style?: {};
  className?: string;
}

const schema = new SimpleSchema({
  email: {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
  },
});

const Text = styled.p`
  font-weight: 600;
  font-size: 18px;
  line-height: 22px;
  margin-bottom: 20px;
`;

const schemaBridge = new SimpleSchema2Bridge(schema);
const HomeNotLoggedIn = () => {
  const sendLoginEmail = useSendLoginEmail();
  const [processing, setProcessing] = useState(false);
  const [sent, setSent] = useState(false);
  const ref = useRef<any>();

  return (
    <ContentPadding>
      {!sent ? (
        <>
          {hasSignup ? (
            <>
              <Link href="/signup/steps/1">
                <LinkButton>Sign Up</LinkButton>
              </Link>
              <Spacer unit={3} />
            </>
          ) : null}
          <p>
            Already signed up? Please submit your email to receive a login link.
          </p>
        </>
      ) : (
        <p>Please check your inbox (and your spam folder if necessary).</p>
      )}
      <AutoForm
        ref={ref}
        schema={schemaBridge}
        disabled={processing}
        onSubmit={async (model) => {
          setProcessing(true);

          try {
            await sendLoginEmail(model.email);
            setSent(true);
            ref?.current?.reset();
          } catch (e) {
            // TODO: React notify
            alert(
              "Email address not found: you need to be signed up for this Hackathon.",
            );
          } finally {
            setProcessing(false);
          }
        }}
      />
    </ContentPadding>
  );
};

const boldText : CSSProperties = {
    fontWeight: 'bold'
}

const Home: React.FC<HomeProps> = ({ style, className }) => {
  const { me, myTeam, loading, hasProject } = useMyTeam();

  if (loading) {
    return <p style={{ padding: "1rem" }}>Loading…</p>;
  }

  if (!me) {
    return <HomeNotLoggedIn />;
  }

  return (
    <ContentPadding>
      <WelcomeMessage />
      <Text>
        This is your hackathon dashboard. As the event progresses, you will
        find all your info here.
      </Text>
      {hasSlack ? (
        <>
          <BigLink
            href={publicRuntimeConfig.SLACK_SIGNUP_URL}
            target="_blank"
            icon={<ArrowRight />}
          >
            Join Slack
          </BigLink>
        </>
      ) : null}
      {hasMattermost ? (
        <>
          <BigLink
            href={publicRuntimeConfig.MATTERMOST_SIGNUP_URL}
            target="_blank"
            icon={<ArrowRight />}
          >
            Join Mattermost
          </BigLink>
	  <InternalBigLink href="/mattermost-channels">Mattermost Channels</InternalBigLink>
        </>
      ) : null}
      {myTeam ? (
        <>
          <InternalBigLink href="/team">Your team</InternalBigLink>
          {myTeam.challengeSelected ? (
            <>
              <InternalBigLink href="/team/challenge">
                Your team's challenge
              </InternalBigLink>
              {!hasProject ? (
                <InternalBigLink href="/team/project/edit">
                  Start team project
                </InternalBigLink>
              ) : (
                <InternalBigLink href="/team/project">
                  Project submission
                </InternalBigLink>
              )}
            </>
          ) : (
            <>
              <InternalBigLink href="/team/challenge">
                Vote for your preffered challenge
              </InternalBigLink>
            </>
          )}
          {hasFeedback ? (
            <>
              <BigLink
                href={publicRuntimeConfig.FEEDBACK_URL}
                target="_blank"
                icon={<ArrowRight />}
              >
                Provide Feedback
              </BigLink>
            </>
          ) : null}
          <Spacer unit={1} />
        </>
      ) : (
        <>
          <Spacer unit={2} />
          <p>The challenges are extracted from the ideas submitted (see{" "}
            <a href="https://webfest.cern/ideas/" target="_blank">here</a>).
            You can also browse them using the link below. 
          </p>
          <Spacer unit={1} />
          <p>
            You are kindly
            requested to choose one challenge to work on during the Webfest.{" "}
            <span style={boldText}>You can adjust your choice until {publicRuntimeConfig.CHALLENGE_SELECTION_UNTIL}</span>, at 
            which point we will build the teams based on your challenge choices.
          </p>
	  <InternalBigLink href="/challenges">Challenges</InternalBigLink>
          <Spacer unit={2} />
          <ChallengeSelect />
        </>
      )}
      <Spacer unit={2} />
      <Heading>Event Schedule</Heading>
      <Schedule />
      <Spacer unit={4} />
    </ContentPadding>
  );
};

export default Home;
