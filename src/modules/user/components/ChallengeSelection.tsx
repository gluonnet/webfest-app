import React, { Fragment } from "react";
import styled from "styled-components";
import Spacer from "../../layout/components/Spacer";
import { useMutation, gql } from "@apollo/client";
import { Select, MenuItem } from "@material-ui/core";

import SimpleSchema from "simpl-schema";
import SimpleSchema2Bridge from "uniforms-bridge-simple-schema-2";

import { UpdateUserChallenge, UpdateUserChallengeVariables } from "./types/UpdateUserChallenge"
import useChallenges from "../../challenge/hooks/useChallenge";
import useMe from "../hooks/useMe";
import useMyTeam from "../hooks/useMyTeam";

const Base = styled.div``;

const Text = styled.p`
  color: red;
  font-weight: 600;
`;



const schemaDefinition = {
  user_id: {
    type: String,
  },
  challenge_id: {
    type: String,
  },
};
const schema = new SimpleSchema(schemaDefinition as any);
const schemaBridge = new SimpleSchema2Bridge(schema);

export const UserDetails = gql`
  fragment UserDetails on User {
    id
    preferredChallenge { id }
  }
`;

export const UpdateUserChallengeMutation = gql`
  mutation UpdateUserChallenge($userId: String!, $challengeId: String!) {
    updateUserChallenge(user_id: $userId, challenge_id: $challengeId)
  }
`;

interface SelectItemProps {
  isCustomized: boolean
}

const StyledSelect = styled(Select)<SelectItemProps>``;

export interface ChallengeSelectListProps {
  value?: string;
  onChange: (challengeId: string) => void;
}

interface ChallengeSelectProps {

}

export const ChallengeSelect: React.FC<ChallengeSelectProps> = ({

}) => {
  const [challengeId, setChallengeId] = React.useState("UserChallenge");
  const topicId = "";
  const { challenges } = useChallenges({ topicId });
  const { me } = useMe();
  const userChallengeId = me?.preferredChallengeId;

  const [updateMutation] = useMutation<UpdateUserChallenge, UpdateUserChallengeVariables>(
    UpdateUserChallengeMutation,
  );

  const handleChange = async (event) => {
    const selectedChallengeId = event.target.value;
    await updateMutation({
      variables: {
        userId: me.id,
        challengeId: selectedChallengeId,
      },
    });
    // alert('Challenge updated in database!');
    // This is a Hack!
    setChallengeId(event.target.value as string);
    window.location.reload(false);
  };

  return (
    <>
      <Text>Choose the idea you want to work on during the Webfest by clicking on the dropdown menu below. This will be used to build teams.</Text>
      <StyledSelect
        value={userChallengeId}
        onChange={handleChange}
        style={{ width: "100%" }}
      >
        {challenges?.map(c => (
          <MenuItem key={c.id} value={c.id}>
            {c.title}
          </MenuItem>
        ))}
      </StyledSelect>
    </>
  );
};

const ChSelLink = styled.a`
  color: red;
  font-weight: 600;
  position: relative;
  top: -20px;
  border: 1px solid red;
  background-color: red;
  border-style: solid;
  color: white;
`;

interface ChallengeSelectLinkProps {
  challenge_id: string
}

export const ChallengeSelectLink: React.FC<ChallengeSelectLinkProps> = ({
  challenge_id,
}) => {
  const [challengeId, setChallengeId] = React.useState("UserChallenge");
  const topicId = "";
  const { challenges } = useChallenges({ topicId });
  const { me } = useMe();
  const userChallengeId = me?.preferredChallengeId;

  const [updateMutation] = useMutation<UpdateUserChallenge, UpdateUserChallengeVariables>(
    UpdateUserChallengeMutation,
  );

  const handleClick = async (event) => {
    const selectedChallengeId = event.target.getAttribute("value");
    await updateMutation({
      variables: {
        userId: me.id,
        challengeId: selectedChallengeId,
      },
    });
    // alert('Challenge updated in database!');
    // This is a Hack!
    window.location.reload(false);
  };

  return (
    <>
      { me?.id ? (
        <ChSelLink onClick={handleClick} value={challenge_id} href="#">
          Select this challenge
        </ChSelLink>
      ) : null }
    </>
  );
};


const SelectedChText = styled.div`
  font-weight: 600;
  font-size: 20px;
  line-height: 24px;
  margin-top: 24px;
  padding: 24px;
  background-color: #f4e456;
`;

interface ChallengeSelectedProps {
}

export const ChallengeSelected: React.FC<ChallengeSelectedProps> = ({
}) => {
  const topicId = "";
  const { challenges } = useChallenges({ topicId });
  const { me } = useMe();
  const userChallengeId = me?.preferredChallengeId;
  const userChallengeTitle = challenges?.find( (c) => c.id == userChallengeId )?.title;

  const { myTeam, loading, hasProject } = useMyTeam();

  return (
    <>
      { (me?.preferredChallengeId && !myTeam) ? (
        <SelectedChText>
          Your selected challenge is:
          <p style={{ padding: "12px" }}>{userChallengeTitle}</p>
        </SelectedChText>
      ) : null}
    </>
  );
};
