/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: UserDetails
// ====================================================

export interface UserDetails_preferredChallenge {
  __typename: "Challenge";
  id: string;
}

export interface UserDetails {
  __typename: "User";
  id: string;
  preferredChallenge: UserDetails_preferredChallenge | null;
}
