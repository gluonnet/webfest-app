/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UpdateUserChallenge
// ====================================================

export interface UpdateUserChallenge {
  updateUserChallenge: boolean;
}

export interface UpdateUserChallengeVariables {
  userId: string;
  challengeId: string;
}
