import React from "react";
import styled from "styled-components";
import { ChallengeDetails as ChallengeDetailsT } from "../../../fragments/types/ChallengeDetails";
import Heading from "../../layout/components/Heading";
import Spacer from "../../layout/components/Spacer";
import Linkify from "react-linkify";
import { ChallengeSelectLink } from "../../user/components/ChallengeSelection";
import useMyTeam from "../../user/hooks/useMyTeam";


const Base = styled.div``;

export interface ChallengeDetailsProps {
  style?: {};
  className?: string;
  challenge: ChallengeDetailsT;
}
const componentDecorator = (href, text, key) => (
  <a href={href} key={key} target="_blank" rel="noopener noreferrer">
    {text}
  </a>
);

const Text = styled.p`
  white-space: pre-line;
`;

const ChallengeDetails: React.FC<ChallengeDetailsProps> = ({
  style,
  className,
  challenge
}) => {
  const { myTeam, loading, hasProject } = useMyTeam();

  return (
    <Base style={style} className={className}>
    {false ? ( // False for CERN Webfest
      <Linkify componentDecorator={componentDecorator}>
        <Heading>{challenge.title}</Heading>
        <Text>{challenge.challenge}</Text>
        <Spacer />
        <Heading>Context</Heading>
        <Text>{challenge.context}</Text>
        <Spacer />
        <Heading>Solution</Heading>
        <Text>{challenge.solution}</Text>
        <Spacer />
        <Heading>Resources</Heading>
        <Text>{challenge.resources}</Text>
      </Linkify>
    ) : (
      <Linkify componentDecorator={componentDecorator}>
        <Heading>{challenge.title}</Heading>
        <Spacer />
        {myTeam ? null : (<ChallengeSelectLink challenge_id={challenge.id}/>)}
        <Heading>Description</Heading>
        <Text>{challenge.context}</Text>
        <Spacer />
        <Heading>Weekend Goals</Heading>
        <Text>{challenge.challenge}</Text>
        <Spacer />
        <Heading>Skills being sought</Heading>
        <Text>{challenge.solution}</Text>
        <Spacer />
        <Heading>Resources</Heading>
        <Text>{challenge.resources}</Text>
      </Linkify>
    )}
    </Base>
  );
};

export default ChallengeDetails;
