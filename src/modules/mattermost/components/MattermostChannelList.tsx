import React from "react";

import styled from "styled-components";
import Spacer from "../../layout/components/Spacer";
import { LinkButton } from "../../core/components/Button";

import getConfig from "next/config";

import useHackerTopics from "../../mentors/hooks/useHackerTopics";

const { publicRuntimeConfig } = getConfig();


const Base = styled.div``;

const Text = styled.div`
  font-weight: 600;
  font-size: 14px;
  line-height: 24px;
  margin-bottom: 20px;
`;

export interface MattermostChannelListProps {
    style?: {};
    className?: string;
  }

const MattermostChannelList: React.FC<MattermostChannelListProps> = () => {
  const { hackerTopics, loading } = useHackerTopics();
  return (
    <Base>
      <Text>Please click on the button below to sign up 
        on {publicRuntimeConfig.HAS_MATTERMOST ? "Mattermost" : "Slack"}.
        Note that you need to sign up on Mattermost before being able to join any of the channels below.
      </Text>
      <LinkButton href={publicRuntimeConfig.MATTERMOST_SIGNUP_URL} target="_blank">
        Join {publicRuntimeConfig.HAS_MATTERMOST ? "Mattermost" : "Slack"}
      </LinkButton>
      <Spacer unit={2} />
      <Text>Some relevant general channels on Mattermost:</Text>
      <LinkButton href={publicRuntimeConfig.MATTERMOST_TEAM_URL + "/channels/announcements"} target="_blank">
        Announcements
      </LinkButton>
      <Spacer unit={1} />
      <LinkButton href={publicRuntimeConfig.MATTERMOST_TEAM_URL + "/channels/helpdesk"} target="_blank">
        Helpdesk
      </LinkButton>
      <Spacer unit={1} />
      <LinkButton href={publicRuntimeConfig.MATTERMOST_TEAM_URL + "/channels/ask-mentor-for-help"} target="_blank">
        Ask a mentor for help
      </LinkButton>
      <Spacer unit={1} />
      <LinkButton href={publicRuntimeConfig.MATTERMOST_TEAM_URL + "/channels/town-square"} target="_blank">
        Town Square
      </LinkButton>
      <Spacer unit={2} />
      <Text>Please click on the buttons to join 
        the {publicRuntimeConfig.HAS_MATTERMOST ? "Mattermost" : "Slack"} 
        channel for the selected topic.</Text>
      {hackerTopics?.map((topic, index) => ( topic?.slack ? (
        <>
          <LinkButton href={topic?.slack.url} target="_blank">
            {topic?.title}
          </LinkButton>
          <Spacer unit={1} />
        </>
        ) : null
      ))}
    </Base>
  )
}


export default MattermostChannelList;

