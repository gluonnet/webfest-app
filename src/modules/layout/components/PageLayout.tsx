import React from "react";
import styled from "styled-components";
import Spacer from "./Spacer";
import ContentWrapper from "./ContentWrapper";
import Header from "./Header";
import { useStoreState } from "../../../model";
import MainNavigation from "../../navigation/components/MainNavigation";

import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();
const hasMattermost = publicRuntimeConfig?.HAS_MATTERMOST== "true";

const Base = styled.div``;

const Content = styled.div`
  padding-bottom: 0;
`;

const InfoBox = styled.div`
  margin-top: 24px;
  padding: 24px;
  background-color: #f4e456;
  line-height: 1.2;
`;

export interface PageLayoutProps {}

const PageLayout: React.FC<PageLayoutProps> = ({ children }) => {
  const showNavigation = useStoreState((s) => s.layout.showNavigation);

  return (
    <Base>
      <Header />
      <ContentWrapper>
        <Content>
          <Spacer />
          {children}
          <Spacer unit={2} />
          {hasMattermost ?
          <InfoBox>
            Any questions? Check out the{" "}
            <a
              href="https://webfest.cern/faq"
              target="_blank"
            >
              FAQs
            </a>{" "}
            or contact{" "}
	    <a href="https://chat.webfest.cern/webfest2022/channels/helpdesk" target="_blank">
              out friendly helpdesk on Mattermost
            </a>
            . Problem joining Mattermost? Please reach out{" "}
	    <a href="mailto:cern.webfest@cern.ch?subject=Question&amp;body=Dear%20CERN%20Webfest%20team%2C%20I%20have%20a%20question%20about%20...">
              by email
            </a>
            .
          </InfoBox>
          :
          <InfoBox>
            Any questions? Check out the{" "}
            <a
              href="https://webfest.cern/faq"
              target="_blank"
            >
              FAQs
            </a>{" "}
            or contact our friendly{" "}
	    <a href="mailto:webfest-team@cern.ch?subject=Question&amp;body=Dear%20CERN%20Webfest%20team%2C%20I%20have%20a%20question%20about%20...">
              helpdesk
            </a>
            .
          </InfoBox>
          }
          <Spacer unit={2} />
	  <p>
            This app is based on the app that was developed for/during the <a href="https://versusvirus.ch">VersusVirus.CH</a> hackathons. The source is availabe on <a href="https://github.com/VersusVirus-Hackathons/VersusVirus-App">GitHub</a>.
          </p>
        </Content>
      </ContentWrapper>
      {showNavigation ? <MainNavigation /> : null}
    </Base>
  );
};

export default PageLayout;
