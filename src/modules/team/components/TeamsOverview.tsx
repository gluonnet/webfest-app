import React, { Fragment } from "react";
import styled from "styled-components";
import Heading from "../../layout/components/Heading";
import Spacer from "../../layout/components/Spacer";
import InternalBigLink from "../../core/components/InternalBigLink";
import ContentPadding from "../../layout/components/ContentPadding";
import ListBackground from "../../core/components/ListBackground";
import ListText from "../../core/components/ListText";
import { Teams } from "../hooks/types/Teams";

import useTeams from "../hooks/useTeams";
import { Team } from "../../../fragments/types/Team";

const Base = styled.div``;

const FlexWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const Label = styled.span`
  padding-right: 6px;
`;

const Info = styled.p`
  padding-top: 24px;
  padding-bottom: 24px;
  color: green;
  font-weight: 600;
`;

export interface TeamDetailListItemProps {
  style?: {};
  className?: string;
  team: Team;
  odd?: boolean;
}

const TeamDetailListItem : React.FC<TeamDetailListItemProps> = ({
  style,
  className,
  team,
  odd,
}) => {
  return(
    <>
      <p>{team.id} {odd} {style} {className}</p>
      <ListBackground style={style} odd={odd} className={className}>
        <FlexWrapper>
          <ListText><Label>Team:</Label>{team.id}</ListText>
          <ListText><Label>Mattermost:</Label>{team.slack.url}</ListText>
        </FlexWrapper>
      </ListBackground>
    </>
  );
};

export interface TeamsOverviewProps {
  style?: {};
  className?: string;
}

const TeamDetails=styled.div`
  padding-top: 24px;
`;

const TeamsOverview: React.FC<TeamsOverviewProps> = ({ style, className }) => {
  const { teams } = useTeams();
  let participantsInTeams = 0;
  let participantsInTeamsWithSubmission = 0;
  teams?.forEach( (t) => {
      participantsInTeams += t.members.length;
      if (t.projects?.length)
        participantsInTeamsWithSubmission += t.members.length;
    }
  )
  return (
    <Base>
    <h1>Teams Overview</h1>
    <Spacer unit={2} />
    <p>Number of teams: {teams?.length}</p>
    <p>Number of participants in teams: {participantsInTeams}</p>
    <Spacer unit={1} />
    <p>Number of teams with a submission: {teams?.filter( t => t.projects?.some( (p) => p.id != null) ).length}</p>
    <p>Number of participants in teams with a submission: {participantsInTeamsWithSubmission}</p>
    <Spacer unit={1} />
    {teams?.map( (team, index) => 
        (<TeamDetails>
        <p>{team.id}: <a href={team.slack.url}>Mattermost channel</a></p>
        <p>Challenge: {team.challengeSelected?.title}</p>
        <p>Members: {team.members.length}</p>
        {team.projects?.some( (p) => p.id != null ) ? (
          <>
          {team.projects.map( (p) => (
            <>
              <p>
                Submission:
                <a target="_blank" href={"/submissions/" + p.id}>{p.title}</a>
              </p>
              {p.videoUrl ? (
                <p>
                  Video URL:
                  <a href={p.videoUrl}>{p.videoUrl}</a>
                </p>
              ) : null }
            </>
          ))}
          </>
        ) : null }
        </TeamDetails>)
    )}
    {teams?.map( (team, index) => {
      <TeamDetailListItem
        key={team.id}
        team={team}
        odd={index % 2 === 0}
      />
    })}
    </Base>
  );
};

export default TeamsOverview;
