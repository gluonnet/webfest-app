/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Teams
// ====================================================

export interface Teams_teams_projects {
  __typename: "Project";
  id: string;
  title: string;
  videoUrl: string | null;
}

export interface Teams_teams_slack {
  __typename: "SlackConversation";
  id: string;
  url: string;
}

export interface Teams_teams_challengeSelected {
  __typename: "Challenge";
  id: string;
  title: string;
}

export interface Teams_teams_members {
  __typename: "User";
  id: string;
}

export interface Teams_teams {
  __typename: "Team";
  id: string;
  projects: Teams_teams_projects[];
  slack: Teams_teams_slack | null;
  challengeSelected: Teams_teams_challengeSelected | null;
  members: Teams_teams_members[];
}

export interface Teams {
  teams: Teams_teams[];
}
