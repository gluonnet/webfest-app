import gql from "graphql-tag";
import { useQuery } from "@apollo/client";
import { Teams } from "./types/Teams";

import { TeamFragment } from "../../../fragments/team";

const QUERY = gql`
  query Teams {
    teams( 
      where: { members: { some: { emailConfirmed: { equals: true } } } }
      orderBy: {id: asc} 
      ){
        ...Team
      }
  }
  ${TeamFragment}
`;

const useTeams = () => {
  const { data, loading, ...rest } = useQuery<Teams>(QUERY, {
    pollInterval: 30000,
    ssr: true,
    fetchPolicy: "cache-and-network"
  });
  return {
    ...rest,
    data,
    teams: data?.teams,
  };
};

export default useTeams;
