# [2.3.0](https://git.panter.ch/manul/vsvirus/compare/v2.2.6...v2.3.0) (2020-06-01)


### Bug Fixes

* faulty import ([63c29b3](https://git.panter.ch/manul/vsvirus/commit/63c29b3c7be89e7d7413c0e830173ffe11733848))
* larger touch target on buttons ([3e28b1a](https://git.panter.ch/manul/vsvirus/commit/3e28b1ae6954c2bcd7670d7dc8f6de99124c5e18))
* move Slack link ([b43143e](https://git.panter.ch/manul/vsvirus/commit/b43143e4bed9f071198390a4d5be3109f6d9dff8))
* only add redirect if URL exists ([c1d9904](https://git.panter.ch/manul/vsvirus/commit/c1d99045f88d739fa53828880fea6deb78eb61d6))


### Features

* add back link ([04ffbfd](https://git.panter.ch/manul/vsvirus/commit/04ffbfd30cb300ba2dbc5675f2022ef8cd6bbd43))
* **#180:** add hard coded colors for each topic, and use it on public pages ([05edfb2](https://git.panter.ch/manul/vsvirus/commit/05edfb2c2921b4aac34d55e80b4cf408d4e22144)), closes [#180](https://git.panter.ch/manul/vsvirus/issues/180)


### Reverts

* Revert "Update schema" ([a29ba79](https://git.panter.ch/manul/vsvirus/commit/a29ba79ec073c485cf0c81e1b73629437a27dbb6))

## [2.2.6](https://git.panter.ch/manul/vsvirus/compare/v2.2.5...v2.2.6) (2020-05-31)


### Bug Fixes

* remove unused file ([35e7db7](https://git.panter.ch/manul/vsvirus/commit/35e7db70859b83c3ca9cfc834eb66ee3d467097e))
* update Storybook that nobody actually uses ([e965d6e](https://git.panter.ch/manul/vsvirus/commit/e965d6e975f3cffbb7b46c345e07c2aa9b78c5a7))

## [2.2.5](https://git.panter.ch/manul/vsvirus/compare/v2.2.4...v2.2.5) (2020-05-30)


### Bug Fixes

* harden the member activation email feature ([8b3ae38](https://git.panter.ch/manul/vsvirus/commit/8b3ae381ef375f6dd95ce7c4c0ae83b6c6a4feee))

## [2.2.4](https://git.panter.ch/manul/vsvirus/compare/v2.2.3...v2.2.4) (2020-05-29)


### Bug Fixes

* pretty header and logo ([d5f0a0e](https://git.panter.ch/manul/vsvirus/commit/d5f0a0ee9429597501c2e5c30aa05d277b84d2e0))

## [2.2.3](https://git.panter.ch/manul/vsvirus/compare/v2.2.2...v2.2.3) (2020-05-28)


### Bug Fixes

* update Slack link ([a14cac4](https://git.panter.ch/manul/vsvirus/commit/a14cac4fa60dfbaa489148f381cd3ac8c584bb3a))
* update Slack link ([2009a94](https://git.panter.ch/manul/vsvirus/commit/2009a94becf8a140a73af72ac127544bc8eaf526))

## [2.2.2](https://git.panter.ch/manul/vsvirus/compare/v2.2.1...v2.2.2) (2020-05-28)


### Bug Fixes

* increase needed pool size ([e852e2c](https://git.panter.ch/manul/vsvirus/commit/e852e2c7e6386f03afd2573f6abc44a770050e89))
* open Slack URL in new window ([a1e02e2](https://git.panter.ch/manul/vsvirus/commit/a1e02e2a280f9154a68028f84d1b522d177b88b5))

## [2.2.1](https://git.panter.ch/manul/vsvirus/compare/v2.2.0...v2.2.1) (2020-05-27)


### Bug Fixes

* remove toLowerCase on undefined value ([40f59b8](https://git.panter.ch/manul/vsvirus/commit/40f59b80658ae9ddf485c1084bc8e8b98aef3d9d))
* remove trailing slash from signup link ([9d1cd6c](https://git.panter.ch/manul/vsvirus/commit/9d1cd6c79db052dbb54063735d1796faa1ef20db))

# [2.2.0](https://git.panter.ch/manul/vsvirus/compare/v2.1.4...v2.2.0) (2020-05-27)


### Bug Fixes

* Add GraphQL query example ([04281f9](https://git.panter.ch/manul/vsvirus/commit/04281f9ec58f4434637cb3f5beed948d2e3fc819))
* max width for thumbnail in overview ([7a502e8](https://git.panter.ch/manul/vsvirus/commit/7a502e8ef16e942f0ab9ff0522f97cff15ebed7a))


### Features

* **#181:** style adjustments ([27e4612](https://git.panter.ch/manul/vsvirus/commit/27e4612eb3f3b52d5ebb7225599c0973e4b62fa5)), closes [#181](https://git.panter.ch/manul/vsvirus/issues/181)
* **submission:** add isPubished column ([85fdc84](https://git.panter.ch/manul/vsvirus/commit/85fdc844b46b555343e94107d3fbd4d7ce93e48b))

## [2.1.4](https://git.panter.ch/manul/vsvirus/compare/v2.1.3...v2.1.4) (2020-05-25)


### Bug Fixes

* replace helpdesk email address ([bc3fcfb](https://git.panter.ch/manul/vsvirus/commit/bc3fcfbedc18f9d6d616c99443d1bafabce09c22))

## [2.1.3](https://git.panter.ch/manul/vsvirus/compare/v2.1.2...v2.1.3) (2020-05-22)


### Bug Fixes

* Disable seeding the topics ([e476fd2](https://git.panter.ch/manul/vsvirus/commit/e476fd218a6032b53c2f762872f36a8fa9eff56a))

## [2.1.2](https://git.panter.ch/manul/vsvirus/compare/v2.1.1...v2.1.2) (2020-05-21)


### Bug Fixes

* bump ([c258f97](https://git.panter.ch/manul/vsvirus/commit/c258f97079f64b5b884cc5dce27f2ff394b208d5))

## [2.1.1](https://git.panter.ch/manul/vsvirus/compare/v2.1.0...v2.1.1) (2020-05-20)


### Bug Fixes

* rename resume cookie to avoid conflicts with older broken cookies ([d633e26](https://git.panter.ch/manul/vsvirus/commit/d633e2608c29248a6b423bec0fc9010e77731816))

# [2.1.0](https://git.panter.ch/manul/vsvirus/compare/v2.0.1...v2.1.0) (2020-05-19)


### Bug Fixes

* project overview when no thumbnail is present ([1f40aa8](https://git.panter.ch/manul/vsvirus/commit/1f40aa8563f5c8a4c2821e66322310f683a94496))
* **submittted projoect:** show video as link and only show titles if content is present ([610b504](https://git.panter.ch/manul/vsvirus/commit/610b504bb53fbb9a2b4a037e47038e6400865fe2))
* add missing migration ([47234a3](https://git.panter.ch/manul/vsvirus/commit/47234a361e7b1de1a388fe91416be2fc42f7083c))
* **signup:** user photo ([a0e4a09](https://git.panter.ch/manul/vsvirus/commit/a0e4a09cb21c3fd986a695c60103d1c1eade46ec))


### Features

* **support:** change to email ([6fb800d](https://git.panter.ch/manul/vsvirus/commit/6fb800d3df86ec365de79293b3cb1c5ae0977e4e))
* add label to thumbnail ([3dc1193](https://git.panter.ch/manul/vsvirus/commit/3dc1193e377c54c892a0f0507cc0f8ae3f8b7b87))
* add prefered challenges to users ([53ae775](https://git.panter.ch/manul/vsvirus/commit/53ae775f778d324c2cfd069ab55da97691b2e94f))
* add public page for individual project ([2a7542d](https://git.panter.ch/manul/vsvirus/commit/2a7542d38b609941da843038fe1f4a42878c3186))
* remove link to projkect from team and team-challenge pages ([5279c72](https://git.panter.ch/manul/vsvirus/commit/5279c72e9ac3c12836b98629191105c876c73a8f))
* remove signup buttton ([cde5055](https://git.panter.ch/manul/vsvirus/commit/cde50553bb1fe61e15d4d22749a55760453e9843))
* **#140:** flow adjustments ([42167b2](https://git.panter.ch/manul/vsvirus/commit/42167b217be1d711d0106a099c9b342012bc9c8f)), closes [#140](https://git.panter.ch/manul/vsvirus/issues/140)

## [2.0.1](https://git.panter.ch/manul/vsvirus/compare/v2.0.0...v2.0.1) (2020-05-15)


### Bug Fixes

* allow to delete users in admin ([11f9494](https://git.panter.ch/manul/vsvirus/commit/11f94944ab95d2483839bd3d5499b18899ebafb4))

# [2.0.0](https://git.panter.ch/manul/vsvirus/compare/v1.22.0...v2.0.0) (2020-05-14)


### Bug Fixes

* some type errors ([9832afb](https://git.panter.ch/manul/vsvirus/commit/9832afbeff566fcbb7b040a89811829b37d458fb))


### Features

* upgrade prisma to beta (needs manual steps) ([5b84515](https://git.panter.ch/manul/vsvirus/commit/5b84515654df7ed2852d35dffa164fbab9ae716b))


### BREAKING CHANGES

* there is not a automatic migration from prisma2-preview prisma2-beta. See https://github.com/prisma/migrate/issues/408

but there is a way to do it:

1.) make sure to checkout the last version BEFORE this commit here (version 1.x.x)

2.) `yarn install`

3.) make sure that your DB is up to date (run `yarn prisma:migrate:up`)

4.) checkout this tag (should be `2.0.0`)

5.) `yarn install`

6.) run `yarn scripts:updateToPrismaBeta`

7.) run `yarn prisma:migrate:up`

Now it should work again

# [1.22.0](https://git.panter.ch/manul/vsvirus/compare/v1.21.1...v1.22.0) (2020-05-14)


### Bug Fixes

* Document conventions in Readme ([3ca75ad](https://git.panter.ch/manul/vsvirus/commit/3ca75adb72eb4da3951f919cbfacf0dd6f6b74bb))
* mandatory challenge and team on project breaks migration ([3e4176b](https://git.panter.ch/manul/vsvirus/commit/3e4176b9de8dfee688258a1b2795194cd7c3d9b2))
* nvmrc file ([246bd85](https://git.panter.ch/manul/vsvirus/commit/246bd85f9f49a76febe2db540f0402bf79d33016))


### Features

* **submission:** add new fields ([9222969](https://git.panter.ch/manul/vsvirus/commit/9222969948976110d8e94579cf08c7beb3b5a24e))
* add public project page ([fdc3fc1](https://git.panter.ch/manul/vsvirus/commit/fdc3fc1c36809e40c53e0309f340157e2ce79641))
* anonymize one user ([3de9238](https://git.panter.ch/manul/vsvirus/commit/3de92382ae33f5609be245bc252f657be3839d32))
* **create project:** add thumbnail and tagline fields ([e583512](https://git.panter.ch/manul/vsvirus/commit/e5835127b2a2628798532b043384eb47a59be4bd))
* **import:** define new schema for submissions ([d064aeb](https://git.panter.ch/manul/vsvirus/commit/d064aeb5b06db8d4e9b70a8a52ddd0dd7185ed79))
* **import:** import submissions as a project ([9bdebbf](https://git.panter.ch/manul/vsvirus/commit/9bdebbf2a74739fb9e94fab4f26d3ce0d0490e37))
* **import:** install csvtojson parser ([fe02c81](https://git.panter.ch/manul/vsvirus/commit/fe02c8176d3218285c64547121233a5eb6237055))

## [1.21.1](https://git.panter.ch/manul/vsvirus/compare/v1.21.0...v1.21.1) (2020-04-05)


### Bug Fixes

* link to /team/project on dashbord ([3955c95](https://git.panter.ch/manul/vsvirus/commit/3955c9562198d1ad7f779c302314087b448bd806))
* text on dashboard and submission page ([4605765](https://git.panter.ch/manul/vsvirus/commit/4605765d111c7cdcd476df8d1b2a31405fd7958d))

# [1.21.0](https://git.panter.ch/manul/vsvirus/compare/v1.20.0...v1.21.0) (2020-04-05)


### Features

* **devpost:** submit devpost url ([69903c1](https://git.panter.ch/manul/vsvirus/commit/69903c142714e50d39c71dd9cc54e0e9e3fe0fb2))
* **user profile:** create page and crud ([f8ce2a1](https://git.panter.ch/manul/vsvirus/commit/f8ce2a1d512345798a35e5ada9fa9961cf30cb37))
* fix challenges seed ([0628437](https://git.panter.ch/manul/vsvirus/commit/0628437033893bceae3a762d1f14de6554708bf2))

# [1.20.0](https://git.panter.ch/manul/vsvirus/compare/v1.19.0...v1.20.0) (2020-04-04)


### Bug Fixes

* **project:** dont' poll forever ([b242f79](https://git.panter.ch/manul/vsvirus/commit/b242f791077a4c981ead757e56a38363818da3d7))
* **project:** update project when logged in ([e7d60f6](https://git.panter.ch/manul/vsvirus/commit/e7d60f6cbf1cae00d3606b80769d807bb3838c1e))


### Features

* **project:** allow create project when you are logged in ([a0fd4ea](https://git.panter.ch/manul/vsvirus/commit/a0fd4ea5b0a52636706b9ed35942bf7848ff12f6))

# [1.19.0](https://git.panter.ch/manul/vsvirus/compare/v1.18.0...v1.19.0) (2020-04-04)


### Bug Fixes

* wrong link ([3740293](https://git.panter.ch/manul/vsvirus/commit/3740293bf8a272f2a3ef7337c1c6c736802c672c))


### Features

* use textfield for team user relation and reload after logout ([cfc343a](https://git.panter.ch/manul/vsvirus/commit/cfc343abbbd445c0ae49831e8e8043aa8e99f435))

# [1.18.0](https://git.panter.ch/manul/vsvirus/compare/v1.17.0...v1.18.0) (2020-04-03)


### Features

* fix porject create ([ca76c90](https://git.panter.ch/manul/vsvirus/commit/ca76c907e9421ad468cdb614c8637b7a67dfee6e))

# [1.17.0](https://git.panter.ch/manul/vsvirus/compare/v1.16.0...v1.17.0) (2020-04-03)


### Features

* add text improvements ([25c312a](https://git.panter.ch/manul/vsvirus/commit/25c312a508e2f231232edb6f9955f0670b5ccbfe))

# [1.16.0](https://git.panter.ch/manul/vsvirus/compare/v1.15.10...v1.16.0) (2020-04-03)


### Bug Fixes

* reducd queries ([e082346](https://git.panter.ch/manul/vsvirus/commit/e08234629eb671b14b9c79ba0c3f855e4b553f21))


### Features

* add text improvements ([1bea980](https://git.panter.ch/manul/vsvirus/commit/1bea980a932462e6dc7e7d3073962ead1aa26650))

# [1.13.0](https://git.panter.ch/manul/vsvirus/compare/v1.12.0...v1.13.0) (2020-04-02)


### Bug Fixes

* broken project links ([95fe902](https://git.panter.ch/manul/vsvirus/commit/95fe90224c3ae8f6d7c04ecf6dd8884d5fda0645))
* challenge list item ([e92218e](https://git.panter.ch/manul/vsvirus/commit/e92218e954be4e02576084f372a19d1ace9a43e9))
* fix ([1e4ca72](https://git.panter.ch/manul/vsvirus/commit/1e4ca728fe91faec6eb1c4b9c2f2af69d113adf8))
* save ([4042a89](https://git.panter.ch/manul/vsvirus/commit/4042a89082fbff746d7a0bf7c73c7ab34589622a))


### Features

* add correct routing ([73edb5a](https://git.panter.ch/manul/vsvirus/commit/73edb5a9d47aa8d00f9e0be1b8181de1600b63dc))
* add more text changes ([a4c2dc3](https://git.panter.ch/manul/vsvirus/commit/a4c2dc30dd98302414c2eb7e13f20c6aead88592))
* add text update ([7e4b6c2](https://git.panter.ch/manul/vsvirus/commit/7e4b6c219f8c59a02fce855a9fb2d52da0e805f0))
* add topic ([889d01b](https://git.panter.ch/manul/vsvirus/commit/889d01baedd7fd546f2508c5e2e0f13add4dbe43))
* add underline ([9cd8819](https://git.panter.ch/manul/vsvirus/commit/9cd881921dfb5e020aa04f3107c14139ee3c153e))
* basi detail page ([b51ce90](https://git.panter.ch/manul/vsvirus/commit/b51ce90ce8d67f6a14f7392243a3284ac8384ac6))
* broadcast ([e794a85](https://git.panter.ch/manul/vsvirus/commit/e794a850ce17c1eda5a155a3b581de81d2039271))
* broadcast ([997440b](https://git.panter.ch/manul/vsvirus/commit/997440b4c2cf27beb3e3d9518d01a8713d317908))
* disable login ([434a9f6](https://git.panter.ch/manul/vsvirus/commit/434a9f6feb1f08da0d2da32edde1ab179a28748e))
* fix routing ([d246a69](https://git.panter.ch/manul/vsvirus/commit/d246a69616119cad1266b0c325756a3ea27f8e68))
* gugus ([4dd9d08](https://git.panter.ch/manul/vsvirus/commit/4dd9d08b0a099a7107a70a9e44673dbf813f92db))
* ich weiss ned so genau wasi da sell schribe ([bdc596d](https://git.panter.ch/manul/vsvirus/commit/bdc596d8fbdfcfaa965c3ea4be8c5abd35a1f507))
* remove feature/project-detail-page ([1ed4e95](https://git.panter.ch/manul/vsvirus/commit/1ed4e953365a15f001766e65907a2efa32cdd530))
* remove link on bottom ([3d42b6c](https://git.panter.ch/manul/vsvirus/commit/3d42b6c9267cecdf2bb477d04dfe6721310f8860))
* update home intro text ([f642e82](https://git.panter.ch/manul/vsvirus/commit/f642e82a81c595b516dc940f558f762992db5b40))
* use basic link ([ba2d962](https://git.panter.ch/manul/vsvirus/commit/ba2d962fb8b5c0a4874ec397c9fc3aafb9258544))
* waba daba dab dab ([d862a3f](https://git.panter.ch/manul/vsvirus/commit/d862a3ff4b78cf28ea26437de86178dcd8833794))

# [1.12.0](https://git.panter.ch/manul/vsvirus/compare/v1.11.0...v1.12.0) (2020-04-02)


### Features

* mutation to delete votes ([ac47155](https://git.panter.ch/manul/vsvirus/commit/ac4715571256bdf3bc353cb89b1056d202923d1e))

# [1.11.0](https://git.panter.ch/manul/vsvirus/compare/v1.10.0...v1.11.0) (2020-04-02)


### Bug Fixes

* <3 ([ee66900](https://git.panter.ch/manul/vsvirus/commit/ee6690025e6bb20d9b4e53cbcc559c62b8031d94))
* be more graceful in parsing internal links ([a83dbb2](https://git.panter.ch/manul/vsvirus/commit/a83dbb2f5e5c1201428fc55f4bf2294e65e50162))
* bild upload ([1d48a56](https://git.panter.ch/manul/vsvirus/commit/1d48a567d6e3ad71b86445a707a549b24ca47725))
* bump ([de93cb8](https://git.panter.ch/manul/vsvirus/commit/de93cb8676350b358286c0f3e9572a9c2fe7f2b5))
* can't create mentor in admin ([2fa30ad](https://git.panter.ch/manul/vsvirus/commit/2fa30ada71484fec6612d29c4fb5641e1b16c159))
* date input on schedule ([274896a](https://git.panter.ch/manul/vsvirus/commit/274896ae19771f80e7381f0a8adfc68ee049d0f6))
* does not show message ([7235446](https://git.panter.ch/manul/vsvirus/commit/72354462b223cfee3738c2a5faf8429cee5bef99))
* don't throw when slack channel creation fails ([f1d7e5c](https://git.panter.ch/manul/vsvirus/commit/f1d7e5c21cac1ac978624b3a88b33566013deab9))
* fix ([c50be74](https://git.panter.ch/manul/vsvirus/commit/c50be749402d5d03dd12c044ef5ab058678053ee))
* fixes ([7538b39](https://git.panter.ch/manul/vsvirus/commit/7538b3923bfdaaddfd822055e335bfed8b5b1a55))
* improve team view ([bdadaa2](https://git.panter.ch/manul/vsvirus/commit/bdadaa2dd4107e51f62c413d97f346d56775174f))
* mentor topics ([a4ca26e](https://git.panter.ch/manul/vsvirus/commit/a4ca26ed884504814356da94f138cc9f1fb234d8))
* notice ([32f63e4](https://git.panter.ch/manul/vsvirus/commit/32f63e457f0255526ecaaec71312e13d56800b49))
* recreateAllTeams throws error ([f2ac6e4](https://git.panter.ch/manul/vsvirus/commit/f2ac6e45224890535d0106fc6d68dff96cd02e31))
* remove connect button ([6f85b13](https://git.panter.ch/manul/vsvirus/commit/6f85b13cee268e1a33f590f4931215dd33ed2d93))
* remove console.log and use schema clean ([2f7fb8e](https://git.panter.ch/manul/vsvirus/commit/2f7fb8e91bfe4f68cf7d47372f08795a4008d8b2))
* topic assigment ([10fdfc0](https://git.panter.ch/manul/vsvirus/commit/10fdfc0f617aac8fef80808507add84cdfab67b7))
* type error ([7401587](https://git.panter.ch/manul/vsvirus/commit/740158741adde25adae89d7aec5568938ab6d2ac))
* type errors ([ae84cbb](https://git.panter.ch/manul/vsvirus/commit/ae84cbbb6ec893ce8219abc5555dcb2c059b63cc))
* typing ([b14e13f](https://git.panter.ch/manul/vsvirus/commit/b14e13f4e501ab11322e5b5c4134d9c413127045))
* typings ([b4708c6](https://git.panter.ch/manul/vsvirus/commit/b4708c66223c8be8823f90221cf2a55251436239))
* typo ([616a25e](https://git.panter.ch/manul/vsvirus/commit/616a25ef0acf0895c4e998eb649f34bccae4c235))
* warning ([e49f3af](https://git.panter.ch/manul/vsvirus/commit/e49f3af5ff0badf17dbaa6637a5998a626913d14))


### Features

* add fragment ([e46eac1](https://git.panter.ch/manul/vsvirus/commit/e46eac1c9bbaf8e6c01b1bbf675d1380fd6280a2))
* add project link ([87fada9](https://git.panter.ch/manul/vsvirus/commit/87fada9acd3639917b9fd2ec97600f6f1bb0b16c))
* add submit button ([7531166](https://git.panter.ch/manul/vsvirus/commit/75311667417cb795ee4287f693a87a5178aa6c54))
* add success ([5d14850](https://git.panter.ch/manul/vsvirus/commit/5d14850be27f4e327ae955586984c0b8bae4a1b2))
* add tags to teams ([1fb789e](https://git.panter.ch/manul/vsvirus/commit/1fb789e3dfddb61d5a1fb3ba2b0d5815805d0ee6))
* add update/deleteMany ([4aadcc6](https://git.panter.ch/manul/vsvirus/commit/4aadcc654eaf798d1bdfe23368d387f543b79247))
* better image handling using own data types ([e57607a](https://git.panter.ch/manul/vsvirus/commit/e57607a88b370ec1d321780b136949d2dade22da))
* challenge vote (simple versions) ([853e062](https://git.panter.ch/manul/vsvirus/commit/853e0629cc8c18381166c5c195fdb015b1032f16))
* extend seeed data ([e784d66](https://git.panter.ch/manul/vsvirus/commit/e784d6680d37da25cc128c57b50288ecc84f5413))
* gugus ([d53e86a](https://git.panter.ch/manul/vsvirus/commit/d53e86acc72cdf8ca7f9fbf2a1458afac3750dc0))
* improve layout ([2f337eb](https://git.panter.ch/manul/vsvirus/commit/2f337ebfdde26e2c7c448c7d631da8c0b4e3318b))
* jump to day ([e8553c4](https://git.panter.ch/manul/vsvirus/commit/e8553c4545da907074a08b5d87c83d6e1bd0a5c2))
* layout with material ui ([3cfee6e](https://git.panter.ch/manul/vsvirus/commit/3cfee6e3a0a1ae1386b19b8772eff0335b873639))
* load mentors and challenges from spreadsheet ([a1b93ea](https://git.panter.ch/manul/vsvirus/commit/a1b93ea569ed4671452059a01106499281bcaf24))
* primary topic for team ([7ff8964](https://git.panter.ch/manul/vsvirus/commit/7ff8964ffd016a90fa066ec87a4a8fbc24da059c))
* public challenges ([0db1aa1](https://git.panter.ch/manul/vsvirus/commit/0db1aa1874059b4cc6601aca269c1154109d8c0f))
* re add schema clean ([f8fc187](https://git.panter.ch/manul/vsvirus/commit/f8fc1875a5e1419498f61f54fc5399004f19c1dc))
* refetch before save ([16727a7](https://git.panter.ch/manul/vsvirus/commit/16727a7ce9afe6fa8dfc8517af634029fa952b3d))
* remove clean schema ([5f3a2c9](https://git.panter.ch/manul/vsvirus/commit/5f3a2c9790e5e0cc0c5b15d621a3d02d44ac39f1))
* remove material overrides ([d6718a8](https://git.panter.ch/manul/vsvirus/commit/d6718a854d0867dc86484073ad6a0a8fbe0ecf52))
* select challenge winner ([f724d4a](https://git.panter.ch/manul/vsvirus/commit/f724d4af33b781d734b54694438a2395fa1c8d8e))
* set challengeid ([ca0dd0f](https://git.panter.ch/manul/vsvirus/commit/ca0dd0fdaf7b1e81c7610c31776e4dc2b9cc7489))
* teamsUpdated in mutation ([0321f47](https://git.panter.ch/manul/vsvirus/commit/0321f47972f0c64f2d4a0ac33ebb123379ddd6d3))
* unsetAllSelectedChallenges ([fc6c826](https://git.panter.ch/manul/vsvirus/commit/fc6c8262928a137e64f0111780c199aca03fdbd2))

# [1.10.0](https://git.panter.ch/manul/vsvirus/compare/v1.9.0...v1.10.0) (2020-03-31)


### Bug Fixes

*  cross-env missing in pipeline ([e3bed13](https://git.panter.ch/manul/vsvirus/commit/e3bed13b86a6eaee4e9e3695809b07276665c05d))
* array input in some cases ([3284163](https://git.panter.ch/manul/vsvirus/commit/3284163ffffdf6169cf03a5ab076750081a8418b))
* broken build ([b5c0c76](https://git.panter.ch/manul/vsvirus/commit/b5c0c760427a453d587e9e1e7b1826d5d7d91076))
* closing ([3b012a3](https://git.panter.ch/manul/vsvirus/commit/3b012a3a3944572b05d2d93bf4d3c8f1e2603af8))
* english sentences on terms page ([83e814b](https://git.panter.ch/manul/vsvirus/commit/83e814bb3845801c53821f76c17d18f238fdbad7))
* merge conflicts ([3d5a3ac](https://git.panter.ch/manul/vsvirus/commit/3d5a3ac97aaba8ed1c6155620eabff88b1d327d5))
* missing file ([cb6dc60](https://git.panter.ch/manul/vsvirus/commit/cb6dc603f6028e424fee1470a2868f6917db4ff4))
* ordering missing ([d2f2e72](https://git.panter.ch/manul/vsvirus/commit/d2f2e7209f8af061f2e32ed44a70f47ae6566fd4))
* reactor admin-crud ([fd1896c](https://git.panter.ch/manul/vsvirus/commit/fd1896cddf4ade70cd194059c74f0317926cbe58))
* refactor ([3f498b5](https://git.panter.ch/manul/vsvirus/commit/3f498b5e86163c57a43708ed5df3e18a5b27bb33))
* references in admin ([67d6a6d](https://git.panter.ch/manul/vsvirus/commit/67d6a6d9e68bbbd491596776f57d8d0cda4ab8d6))
* references in admin ([40dc0d5](https://git.panter.ch/manul/vsvirus/commit/40dc0d58cfadc30102afaaaa5ba6024cc8f00251))
* remove stories to fix pipeline ([d0cc54e](https://git.panter.ch/manul/vsvirus/commit/d0cc54e4d6a5bedc58a38bd9fea3b59207d6c9d2))
* speed up loading of user by not loading the raw image ([fba2375](https://git.panter.ch/manul/vsvirus/commit/fba237595102805f36e6a90d0b00406635bd656a))
* type on challenge relation ([5faf580](https://git.panter.ch/manul/vsvirus/commit/5faf58092105891ad5118bab8c85d854d20c8852))
* type problem with mentor ([8776752](https://git.panter.ch/manul/vsvirus/commit/87767526f75806873352e34ae24aaa305f495f59))


### Features

* add mentors ([6afe20d](https://git.panter.ch/manul/vsvirus/commit/6afe20d2804f9a38fd7fd37c2413f75d9c694b75))
* change title ([f26fc69](https://git.panter.ch/manul/vsvirus/commit/f26fc69617a0f45ce6b4ea61c6b3a732c7e0c2f3))
* create team ([5913af6](https://git.panter.ch/manul/vsvirus/commit/5913af6a672843aa90708190e347b9b0c8d929cb))
* link to code of conduct on terms page ([67c007d](https://git.panter.ch/manul/vsvirus/commit/67c007d99acc5adf1969d9309e1fc5e62523392e))
* manage mentors ([c48bce8](https://git.panter.ch/manul/vsvirus/commit/c48bce835d9c2f212b97ef4a30adbb6ee55aeb75))
* mentor list and seed ([438f1f9](https://git.panter.ch/manul/vsvirus/commit/438f1f90617c70c8034d7dae4eed8a59d2cea468))
* mentor list item ([0ed58a7](https://git.panter.ch/manul/vsvirus/commit/0ed58a78b997c2f88d3cc41b604b188c304128ce))
* projects ([53bb22a](https://git.panter.ch/manul/vsvirus/commit/53bb22ac929fc7f30620a035dc0ea4be00cbe1fe))
* slack channels for teams ([b7e58a7](https://git.panter.ch/manul/vsvirus/commit/b7e58a7e391edaca154810d43b35cf7fe53f6b1d))
* split components ([f4a2446](https://git.panter.ch/manul/vsvirus/commit/f4a24460db2b0a6eadae825a5e3238783c970df5))
* try import mentors via csv ([1872099](https://git.panter.ch/manul/vsvirus/commit/1872099b797140841f0be326814fe3d792b92cc4))
* update challenge data ([b242822](https://git.panter.ch/manul/vsvirus/commit/b242822c6577cb399ec7fbeb505880ecccf87763))

# [1.9.0](https://git.panter.ch/manul/vsvirus/compare/v1.8.0...v1.9.0) (2020-03-29)


### Bug Fixes

* design ([cae8252](https://git.panter.ch/manul/vsvirus/commit/cae8252325cf4d607967e5e7b4a74a1ab10f88a5))
* fix ([5e0f73d](https://git.panter.ch/manul/vsvirus/commit/5e0f73d532a88876eedd24f9cac7832626831c24))
* update nextjs ([84e7135](https://git.panter.ch/manul/vsvirus/commit/84e7135ee2db061a65c140049bf4f0fe71f93164))


### Features

* better health check ([1c82d8e](https://git.panter.ch/manul/vsvirus/commit/1c82d8e49c93e03b42a41c87a5c942ac01a6550e))

# [1.8.0](https://git.panter.ch/manul/vsvirus/compare/v1.7.2...v1.8.0) (2020-03-28)


### Bug Fixes

* change text ([0f13b36](https://git.panter.ch/manul/vsvirus/commit/0f13b36eda2928253e60211a5917bfad4138d9e9))
* cors ([b1452a5](https://git.panter.ch/manul/vsvirus/commit/b1452a5de16df5a11d253067c3615ff125a98981))
* dont' quit :-D ([75c3439](https://git.panter.ch/manul/vsvirus/commit/75c3439ad6db4428047536e696ad839c01ec8481))
* fix ([cee4eb9](https://git.panter.ch/manul/vsvirus/commit/cee4eb92c6254bb4c1899c077a58acf6ab18f2c6))
* its getting late ([30dae43](https://git.panter.ch/manul/vsvirus/commit/30dae43e2d7c1aea59336fd1968ca120f93a93ea))
* main navigation broken when not logged in ([6552a40](https://git.panter.ch/manul/vsvirus/commit/6552a407d0eaf5bea9318d45a5b6c9cdaaf80c17))
* make User.team optional ([3ba8cc0](https://git.panter.ch/manul/vsvirus/commit/3ba8cc03a0d5d7013c5ab8fa3ed0de10409ba1ee))
* prevent anonymous from fetching teams ([78795b8](https://git.panter.ch/manul/vsvirus/commit/78795b814357ec3d5cd104025251609e90041737))
* seed ([e17e0a4](https://git.panter.ch/manul/vsvirus/commit/e17e0a43e7b8380bbaf8b78294b3c67723b60edf))
* team challenge relations ([7751142](https://git.panter.ch/manul/vsvirus/commit/77511429cff870f3db037931dbfaa5d5c101c60f))
* typing ([2ae2ccd](https://git.panter.ch/manul/vsvirus/commit/2ae2ccdc287cdb5513625002019528a1a19c96f3))


### Features

* allow cors ([cb80fa8](https://git.panter.ch/manul/vsvirus/commit/cb80fa84685852928a3b3b80e7e8b3ecdd7b7b31))
* challenges and teams list ([3391227](https://git.panter.ch/manul/vsvirus/commit/3391227a18327f9f532e67c93de8b3bdbc96ba60))
* dashboard ([258384d](https://git.panter.ch/manul/vsvirus/commit/258384dc0e15fa943ae1b2aa223f76a71d78993d))
* layout ([eea3140](https://git.panter.ch/manul/vsvirus/commit/eea314086367599a2da16a2a7924f167eda400ef))
* member list ([b248aba](https://git.panter.ch/manul/vsvirus/commit/b248aba378fc7f6cea1fd023a67beecabd1246c7))
* mutation for seed ([d40c8ee](https://git.panter.ch/manul/vsvirus/commit/d40c8ee4e3356306331ae3c86a8a7cff0445128e))
* simple admin ([ce83fc8](https://git.panter.ch/manul/vsvirus/commit/ce83fc8d7975426689dfe20c0b186b19df541ba0))
* stuff ([655a047](https://git.panter.ch/manul/vsvirus/commit/655a0471b768c54ac7c1f4dc9310443faa223d5a))
* team and challenges ([67700e4](https://git.panter.ch/manul/vsvirus/commit/67700e4fc171ac8b2c86344eea2a7857ffb2b2ae))
* team making draft ([6a59e0b](https://git.panter.ch/manul/vsvirus/commit/6a59e0b81ada2e6ca80a3128728a8ed4a4dabc5d))
* update react admin ([9e758fa](https://git.panter.ch/manul/vsvirus/commit/9e758fad66ec5eedeeff9360445ba0c6b522abac))

## [1.7.2](https://git.panter.ch/manul/vsvirus/compare/v1.7.1...v1.7.2) (2020-03-25)


### Bug Fixes

* bump ([26fba09](https://git.panter.ch/manul/vsvirus/commit/26fba097ee32b8b272aca2151420f58c13a6738f))

## [1.7.1](https://git.panter.ch/manul/vsvirus/compare/v1.7.0...v1.7.1) (2020-03-25)


### Bug Fixes

* checkbox ([1dd972e](https://git.panter.ch/manul/vsvirus/commit/1dd972ebc6f19e5da7489566467760a472fb8b73))
* fix ([3b51003](https://git.panter.ch/manul/vsvirus/commit/3b510034029a79da2810c37d9edd59b1e951567c))
* remove console log ([87c0783](https://git.panter.ch/manul/vsvirus/commit/87c07836a41eda734cbdbf6c91f15278fc24a50b))
* signup ([6ca2be1](https://git.panter.ch/manul/vsvirus/commit/6ca2be1852bdfbaab72cb6ed5af0a3c34fb5f1a3))

# [1.7.0](https://git.panter.ch/manul/vsvirus/compare/v1.6.0...v1.7.0) (2020-03-24)


### Bug Fixes

* add moment ([99faf47](https://git.panter.ch/manul/vsvirus/commit/99faf4742d3d1ce42510ccfcbc43450474d6c902))
* hydration ([668d9f5](https://git.panter.ch/manul/vsvirus/commit/668d9f536dfeb3930bde4a5514cf33ba837ba8f6))
* lint ([00800f9](https://git.panter.ch/manul/vsvirus/commit/00800f93aab5b5219abc4fbed1cdb1f387b0b990))


### Features

* upload profile foto ([676ad82](https://git.panter.ch/manul/vsvirus/commit/676ad8271710288704703f13e429f16d9bb376df))

# [1.6.0](https://git.panter.ch/manul/vsvirus/compare/v1.5.0...v1.6.0) (2020-03-24)


### Features

* language ([f401f5a](https://git.panter.ch/manul/vsvirus/commit/f401f5a925503c3ac44a4638136b2c42494b86fc))
* multi select lang ([b90d7e7](https://git.panter.ch/manul/vsvirus/commit/b90d7e7522fe942189b0598954c24b38be8c57a0))
* shield ([979d186](https://git.panter.ch/manul/vsvirus/commit/979d186d363901b1e37be9ff4eb9b1d6cb651e94))
* stuff ([9215ffd](https://git.panter.ch/manul/vsvirus/commit/9215ffde162ff004ac486b65faccea05190a335f))

# [1.5.0](https://git.panter.ch/manul/vsvirus/compare/v1.4.0...v1.5.0) (2020-03-24)


### Features

* new domain ([c41bdd4](https://git.panter.ch/manul/vsvirus/commit/c41bdd4bb27c0ef219f17cb4729edbb2927cb488))

# [1.4.0](https://git.panter.ch/manul/vsvirus/compare/v1.3.0...v1.4.0) (2020-03-24)


### Features

* expose crud option (not protected currently) ([f2df3b1](https://git.panter.ch/manul/vsvirus/commit/f2df3b1c975acd7107315e10f81411bbdf225e75))

# [1.3.0](https://git.panter.ch/manul/vsvirus/compare/v1.2.2...v1.3.0) (2020-03-24)


### Bug Fixes

* add missing migrations ([5c11c1a](https://git.panter.ch/manul/vsvirus/commit/5c11c1a8ac2b10fa5a7c7ba3130df3bfe588560a))


### Features

* hackerType multiselect (again) ([a2c112e](https://git.panter.ch/manul/vsvirus/commit/a2c112e31528d68d978d983f5cd664cfb6ee8bc6))

## [1.2.2](https://git.panter.ch/manul/vsvirus/compare/v1.2.1...v1.2.2) (2020-03-23)


### Bug Fixes

* labels ([e50a71f](https://git.panter.ch/manul/vsvirus/commit/e50a71f72cf0e938d669e7c81ef57ef948adc38c))

## [1.2.1](https://git.panter.ch/manul/vsvirus/compare/v1.2.0...v1.2.1) (2020-03-23)


### Bug Fixes

* mobile layout ([2641b3d](https://git.panter.ch/manul/vsvirus/commit/2641b3d85eba3f74249e6cefa4d3a47855e5fe8a))
* rename ([af26378](https://git.panter.ch/manul/vsvirus/commit/af263784e0732383512f89fa736f350f6ea95328))
* update favicon ([9f6447b](https://git.panter.ch/manul/vsvirus/commit/9f6447b4fde752a681670b035f6f5cb48b8f4e1a))

# [1.2.0](https://git.panter.ch/manul/vsvirus/compare/v1.1.0...v1.2.0) (2020-03-23)


### Bug Fixes

* small design fix ([bcb35ae](https://git.panter.ch/manul/vsvirus/commit/bcb35aeed09c212a02631a0e228a3d1e1c898f23))
* style ([61775ba](https://git.panter.ch/manul/vsvirus/commit/61775ba4054dd1ec43030b4129d9686b3b4e4841))


### Features

* logout button ([1bd9e61](https://git.panter.ch/manul/vsvirus/commit/1bd9e61eaf1eb82f1b26d7ee6ff73d915a0f3569))

# [1.1.0](https://git.panter.ch/manul/vsvirus/compare/v1.0.0...v1.1.0) (2020-03-23)


### Bug Fixes

* button ([dcfe45e](https://git.panter.ch/manul/vsvirus/commit/dcfe45e0eea71363aa6054ce230e45a9ec725938))
* button handling ([b5576c2](https://git.panter.ch/manul/vsvirus/commit/b5576c29a7d356b660cd5a81349f8f2a68c08bec))
* center ([b54b75e](https://git.panter.ch/manul/vsvirus/commit/b54b75e2799747bdbb0007bd9c19feffa55c58fe))
* change text ([7bae27d](https://git.panter.ch/manul/vsvirus/commit/7bae27d34c4ebf61a6139da7b0c44944ed3db29a))
* fix ([878908e](https://git.panter.ch/manul/vsvirus/commit/878908e0b6b55462422a59df4979c9668f72d57e))
* fix ([67debb6](https://git.panter.ch/manul/vsvirus/commit/67debb62b7fc1c287cf7bb309f47abe7e20e9294))
* fix ([bd552f4](https://git.panter.ch/manul/vsvirus/commit/bd552f4923b6467623a2da27bda62ba4c6f15ab0))
* fix ([3f953c2](https://git.panter.ch/manul/vsvirus/commit/3f953c26fc2878fc77557ce7b16605b81492e2bf))
* fudi ([183fbed](https://git.panter.ch/manul/vsvirus/commit/183fbed26592c994e8001c850b1d1ee2be590b97))
* int ([0a49c7a](https://git.panter.ch/manul/vsvirus/commit/0a49c7ad231db41bcba1a4e8c65c309db50f6ef2))
* missing import ([1e0a3fc](https://git.panter.ch/manul/vsvirus/commit/1e0a3fc1e91c862b1626b40794db4d55bce68d27))
* stuff ([dfc3ada](https://git.panter.ch/manul/vsvirus/commit/dfc3ada25cebd05330f3ebc447aae593c2295f6c))
* switch to non-alpine. ([6f25d78](https://git.panter.ch/manul/vsvirus/commit/6f25d783fcc2a3e322cd0beaa443126c6a45474e))
* thankyou ([93cdf7c](https://git.panter.ch/manul/vsvirus/commit/93cdf7c0b91e2fef2d4057895cb2a1d871582af8))
* welcome message ([c33155a](https://git.panter.ch/manul/vsvirus/commit/c33155a2a7a108435129978351088369204c2dd2))


### Features

* better design ([f1f71d3](https://git.panter.ch/manul/vsvirus/commit/f1f71d308256ead2075eec75e30c8a5067233f1b))
* change email contents ([63204d4](https://git.panter.ch/manul/vsvirus/commit/63204d4dbb10ccbacc6d6ddc550b5ea7ab3cca0d))
* email ([2b62fc1](https://git.panter.ch/manul/vsvirus/commit/2b62fc12858ac440e1367d6615939ec6054903cf))
* email subject ([8cae27b](https://git.panter.ch/manul/vsvirus/commit/8cae27b61b2a35b9db7649c5c2aa9f3f5d52645d))
* email verify ([a391f43](https://git.panter.ch/manul/vsvirus/commit/a391f43b37ffd59e1679a78d8a54c2e341f3b1da))
* error handling ([067b90b](https://git.panter.ch/manul/vsvirus/commit/067b90bd107b8065483ab18961eb0060920eecae))
* implement the damn thing ([73df307](https://git.panter.ch/manul/vsvirus/commit/73df3073c9d2b0de68f502813eb8c8a89562dc97))
* refactor ([87b78f7](https://git.panter.ch/manul/vsvirus/commit/87b78f7f61441b32330b3aa0124b3b2a8d955153))
* resend email ([842b442](https://git.panter.ch/manul/vsvirus/commit/842b4423954270de64c87578b4e83456c12d9472))
* stuff ([13235f8](https://git.panter.ch/manul/vsvirus/commit/13235f8476a38f04be0513b8d14be0401cc695f8))
* stuff ([e79893a](https://git.panter.ch/manul/vsvirus/commit/e79893a5540f17c9c704742c74d220a210e88e30))
* term and condition ([8f551be](https://git.panter.ch/manul/vsvirus/commit/8f551be475cae82ef1838c3d3b7b8e0da7c6f008))
* token login ([8e4461d](https://git.panter.ch/manul/vsvirus/commit/8e4461daeb838069b893ee83b3f7cdad54e8cda0))
* welcome message ([223c088](https://git.panter.ch/manul/vsvirus/commit/223c0883c33c31fb5498ebd4e51b17951f9eb0a9))

# 1.0.0 (2020-03-22)


### Bug Fixes

* int ([4051eb8](https://git.panter.ch/manul/vsvirus/commit/4051eb8b60ce091b9a71c126cee2c0b44fc31ff5))
* size ([b9c0274](https://git.panter.ch/manul/vsvirus/commit/b9c0274474bc333c573cbafb05046bb973eaf9cf))


### Features

* fix ([be5065a](https://git.panter.ch/manul/vsvirus/commit/be5065ab286c69d9902f6689ff8e4b3d68849e67))
* init ([2bc7b23](https://git.panter.ch/manul/vsvirus/commit/2bc7b237119e413dbe3f978e2b340217962763bb))
* logo n shit ([fb3828b](https://git.panter.ch/manul/vsvirus/commit/fb3828b6d8471077f02fbc749c4bb12429db0f55))
* signnup ([c0aaea7](https://git.panter.ch/manul/vsvirus/commit/c0aaea7a3e0474ffa1dd0ce8c0b2a4b8d40d943a))
