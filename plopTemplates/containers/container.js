import gql from "graphql-tag";
import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { Get{{name}} } from "./types/Get{{name}}";


import {{name}} from '../components/{{name}}';


export interface {{name}}ContainerProps {
  style?: {};
  className?: string;
}

export const GET_{{name}} = gql`
  query Get{{name}} {
    {{camelCase name}} {
      id
      title
    }
  }
`;

const {{name}}Container: React.SFC<{{name}}ContainerProps> = (props) => {
  const { data, error, loading } = useQuery<get{{name}}>(GET_{{name}}, {
    // variables: { id: "xyz" },
  });
  if (loading) {
    return <div>Loading...</div>;
  }
  if (error) {
    return <div>Error! {error.message}</div>;
  }
  
  return <{{name}} {...props} data={data} />
};

export default {{name}}Container;
