{{actionName}}: action((state: {{reducerStateType}}, payload: any) => {
  state.{{{propertyName}}} = payload;
}),
