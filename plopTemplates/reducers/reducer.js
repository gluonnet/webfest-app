import { action } from "easy-peasy";

/* 📌 ACTION-CREATORS */

const initialState = {
  /* 📌 INITIAL-STATE */
};

export type {{reducerStateType}} = typeof initialState;

export default {
  ...initialState,
  /* 📌 ACTION-REDUCERS */
};
