import {storiesOf} from "@storybook/react"; // tslint:disable-line:no-implicit-dependencies
import React from "react";
// import { action } from "@storybook/addon-actions";
import {{name}} from "../{{name}}";

storiesOf("{{moduleName}}/{{name}}", module)
  .add("default view",
  () => (
    <{{name}} />
  )
);
