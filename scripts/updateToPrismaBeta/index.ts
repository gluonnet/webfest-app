import prisma from "../../prisma/prisma";

type Rename = String | { from: string; to: string };
type Table = string;

const renameFields = async (table: string, columns: Rename[]) => {
  const statements = columns.map((c) => {
    const { from, to } =
      typeof c === "string" || c instanceof String
        ? { from: c, to: c + "Id" }
        : c;
    return `
        ALTER TABLE "${table}"
        RENAME COLUMN "${from}" TO "${to}"
        `;
  });

  for (const statement of statements) {
    await prisma.raw(statement);
  }
  return;
};

type Migration = {
  table: Table;
  columns: Rename[];
};

const MIGRATIONS: Migration[] = [
  {
    table: "Challenge",
    columns: ["primaryTopic"],
  },
  {
    table: "Project",
    columns: ["challenge", "team", "thumbnail"],
  },
  {
    table: "Team",
    columns: ["challengeSelected", "primaryTopic"],
  },
  {
    table: "TeamChallengeVote",
    columns: ["team", "user", "challenge"],
  },
  {
    table: "User",
    columns: ["profilePhoto", "team"],
  },
  {
    table: "UserLoginToken",
    columns: ["user"],
  },
  {
    table: "UserResumeToken",
    columns: ["user"],
  },
];

const up = async () => {
  await prisma.raw`DROP TABLE "_Migration"`;
  for (const migration of MIGRATIONS) {
    try {
      await renameFields(migration.table, migration.columns);
    } catch (e) {
      console.error(e);
    }
  }
};

up()
  .then((e) => {
    process.exit(0);
  })
  .catch((e) => {
    // tslint:disable-next-line:no-console
    console.error(e);
    process.exit(1);
  });

export default {};
