import fetch from "node-fetch";
import { objectType } from "@nexus/schema";
import memoize from "memoizee";
// authorizes the versusVirus App to operate
const token = process.env.MATTERMOST_TOKEN;
const apiBase = process.env.MATTERMOST_API;
const team_id = process.env.MATTERMOST_TEAM_ID;

const headers = {
  "Content-Type": "application/json; charset=utf-8",
  Authorization: `Bearer ${token}`,
};
export const createChannel = async (name: string) => {
  const body = {
    name: name,
    display_name: name,
    team_id: team_id,
    type: "O",
  };

  return fetch(apiBase+"/channels", {
    method: "POST",
    body: JSON.stringify(body),
    headers,
  }).then((r) => r.json());
};

export const getAllChannels = memoize(
  async () => {
    const alChannels = await fetch(
      apiBase+"/channels",
      {
        method: "GET",
        headers: headers,
      },
    ).then((r) => r.json());
    return alChannels;
  },
  { promise: true, maxAge: 120000 },
);

export const getChannelName = memoize(
  async(id) => {
    const channelInfo = await fetch(
      apiBase+"/channels/"+id,
      {
        method: "GET",
        headers: headers,
      },
    ).then((r) => r.json());
    return channelInfo.name;
  },
  { promise: true, maxAge: 120000 },
);

/*
// post message in a channel
request.post(
  {
    url: "https://slack.com/api/chat.postMessage",
    form: {
      token,
      channel: channelName,
      text:
        "@channel VersusVirus is inviting you to join a live session in https://youtube.com/wefwefwef",
      parse: "full"
    }
  },
  (err, httpResponse, body) => {
    if (err) {
      console.error(err);
    } else {
      console.log(body);
    }
  }
);
*/
