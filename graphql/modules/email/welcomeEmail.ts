import { stripIndents } from "common-tags";
import sendEmail from "./sendEmail";

export function welcomeEmail(
  to: string,
  name: string,
): {
  to: string;
  text: string;
  subject: string;
} {
  return {
    to,
    subject: "Welcome to the CERN Webfest 2022",

    text: stripIndents`
      Hello ${name},

      Thank you for signing up for the CERN Webfest 2022 and for deciding to contribute with your innovative ideas to hack on the theme 'climate action'.

      Great to have you on board!
      the CERN Webfest Team


    `,
  };
}

export default async function sendWelcomeEmail(to, name) {
  return await sendEmail(welcomeEmail(to, name));
}
