import { objectType } from "@nexus/schema";
import { getChannelName } from "../../mattermost";

const SLACK_DOMAIN = process.env.SLACK_DOMAIN;
const HAS_MATTERMOST = process.env.HAS_MATTERMOST;
const MATTERMOST_TEAM_URL = process.env.MATTERMOST_TEAM_URL;

const getChannelURL = async (id) => {
  const chName = await getChannelName(id);
  return `${MATTERMOST_TEAM_URL}/channels/${chName}`
};

export const SlackConversation = objectType({
  name: "SlackConversation",

  definition(t) {
    t.string("id");
    t.field("url", {
      type: "String",
      resolve({ id }) {
        if(HAS_MATTERMOST) return getChannelURL(id); //`${MATTERMOST_TEAM_URL}/channels/${id}`;
        return `https://${SLACK_DOMAIN}.slack.com/archives/${id}`;
      },
    });
  },
});
