import prisma from "../prisma/prisma";
var fs = require('fs');
import csvParse from "csv-parse/lib/sync";

//import uniq from "lodash/uniq";
//import { getDoc } from "./spreadsheet";

export default async () => {

var csvFilePath = __dirname+'/registrations.csv';

const strcsv = fs.readFileSync(csvFilePath, 'utf-8');


const user_data = csvParse(strcsv, { delimiter: "," });

for (let user_info of user_data) {
  console.log(user_info);
  var first = user_info[2];
  var lastn = user_info[3];
  var email = user_info[4];
 
  await prisma.user.upsert({
    where: {
      email: email,
    },
    create: {
      email: email,
      firstname: first,
      lastname: lastn,
      phoneNumber : "",
      city : "",
      emailConfirmed : true,
    },
    update: {}
  });

}

/*

  console.log("Importing users from CSV");

  var parser = csvParse({delimiter: ','}, function(data, err) {
    console.log(data);
  });

  fs.createReadStream('registrations.csv').pipe(parser).on('data', row => {
   console.log(row);
  }).on('end', () => { console.log("Done importing users"); });

  console.log("Done importing users");

/*

  const mentorsDoc = await getDoc(process.env.GOOGLE_SPREADSHEET_PARTICIPANTS);

  const sheet = mentorsDoc.sheetsByIndex[0];

  const rows = await sheet.getRows({ limit: 100000 });

  for (let row of rows) {
    const mentorTopics = uniq([
      row["Mentorship Category (1st priority)"].trim(),
      row["Mentorship Category (2nd priority)"].trim()
    ]);

    const relatedTopics = await prisma.hackerTopic.findMany({
      where: {
        title: {
          in: mentorTopics
        }
      }
    });
    if (mentorTopics.length !== relatedTopics.length) {
      const missingTopics = mentorTopics.filter(
        topic => !relatedTopics.some(t => t.id === topic)
      );
      console.warn(row.Email, "missing topics:", missingTopics.join("|"));
    }

    await prisma.mentor.upsert({
      where: {
        email: row.Email
      },
      create: {
        email: row.Email,
        name: row["Last Name"],
        skills: row["What skills / expertise could you offer?"],
        linkedin: row["Linkedin or Social Media Profile"],
        languages: row.Language,
        topics: {
          connect: relatedTopics.map(topic => ({
            id: topic.id
          }))
        }
      },
      update: {}
    });
  }
*/

};
