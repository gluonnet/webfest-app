import { isTSPropertySignature } from "@babel/types";
import { clearLine } from "readline";
import project from "../pages/team/project";
import prisma from "../prisma/prisma";
import { getDoc, getBlankDoc } from "./spreadsheet";

const theJurySummaryGSID = "1K3sJA5eFyWj5t9UnHVf2cCgrM3JlrGgK-Av6N0MhaeU";
const theJuryTemplateGSID = "1hvvv4Y9KkOHNutPI7L1tTuLaaiJ5jNbYrkPb4Tdx05Y";

// The maximum number of jury members for the template
const maxJuryMembers = 12;
const theJuryMembers = [
    "François Grey",
    "Ben Segal",
    "Archana Sharma",
    "Charlotte Warakaulle",
    "Jean-Pierre Raymond",
    "Mateusz Malenta",
    "Bilge Demiköz",
    "Ines Knaepper",
];

function getSheetByTitle(doc, sheetTitle) {
    for (let sheet of doc.sheetsByIndex) {
        if( sheetTitle == sheet.title ) {
            return sheet;
        }
    }
    return undefined;
}

async function setJuryMembers(theJurySummaryGSID) {
  console.log("Setting the jury members in the spreadsheet");
  //console.log(theJuryMembers);
  const jurySummaryDoc = await getDoc(theJurySummaryGSID);
  console.log(jurySummaryDoc.title);

  let shInstructions = getSheetByTitle(jurySummaryDoc, "INSTRUCTIONS");
  await shInstructions.loadCells(['A16:G27']);
  let shInfoJury = getSheetByTitle(jurySummaryDoc, "JURY");
  await shInfoJury.loadCells(['D1:AY1']);
  let shInfoSub = getSheetByTitle(jurySummaryDoc, "INFO-Submissions");

  // This is the template for the jury members
  const juryMemberTemplateDoc = await getDoc(theJuryTemplateGSID);
  let shMemberTemplateInfoJury = getSheetByTitle(juryMemberTemplateDoc, "JURY-Template");


  let jury_cnt = 0;
  for (let jM of theJuryMembers) {
      console.log(jM);
      // Clone the jury template and assign document ID
      let cell = shInstructions.getCell(15 + jury_cnt, 2);
      cell.value = jM; // Assigning jury member
      // Getting spreadsheed for judge by ID
      cell = shInstructions.getCell(15 + jury_cnt, 6);
      let judgeDocId = cell.value;
      console.log(judgeDocId);
      jury_cnt++;

      const doc = await getDoc(judgeDocId);
      // Clearing all sheets except first
      for (let i in doc.sheetsByIndex) {
          if (Number(i)>0) await doc.sheetsByIndex[i].delete();
      }
      // Copy instructions spreadsheet
      await shInstructions.copyToSpreadsheet(judgeDocId);
      await doc.sheetsByIndex[0].delete();
      // INSTRUCTIONS should be the new sheet with ID = 0
      // Renaming sheet at destination
      await doc.sheetsByIndex[0].updateProperties({ title: 'INSTRUCTIONS'});
      // Copying JURY - Template sheet
      await shMemberTemplateInfoJury.copyToSpreadsheet(judgeDocId);
      // Updating sheetsByIndex information
      await doc.loadInfo();
      // Renaming sheet at destination
      await doc.sheetsByIndex[1].updateProperties({ title: 'JURY-Template'});
      let judgeSheet = doc.sheetsByIndex[1];
      await judgeSheet.loadCells(['I1']);
      let juryNameCell = judgeSheet.getCellByA1('I1');
      juryNameCell.value = jM;

      await shInfoSub.copyToSpreadsheet(judgeDocId);
      // Updating sheetsByIndex information
      await doc.loadInfo();
      // Renaming sheet at destination
      await doc.sheetsByIndex[2].updateProperties({ title: 'INFO-Submissions'});

      //break;
  }

  /*
  // Not working, updating instead
  // Copying the template for each jury member
  jury_cnt = 0;
  for (let jM of theJuryMembers) {
      console.log(`Creating sheet for jury member ${jM}`);
      const doc = await getBlankDoc();
      await doc.createNewSpreadsheetDocument({ title: 'CERN Webfest 2021 Jury - ' + jM });
      let spreadsheetId = doc.spreadsheetId;
      let cell = shInstructions.getCell(15 + jury_cnt, 6); // Hard-coded
      console.log(doc.title);
      console.log(spreadsheetId);
      cell.value = spreadsheetId;
      break;
  }
   */

  // Updating the summary sheet
  jury_cnt = 0;
  for (let i = 0 ; i < 4 * maxJuryMembers ; i += 4) {
      let cell = shInfoJury.getCell(0, 3 + i);
      if (jury_cnt < theJuryMembers.length) {
          cell.value = theJuryMembers[jury_cnt];
      } else {
          cell.value = "";
      }
      jury_cnt++;
  }

  await shInstructions.saveUpdatedCells();
  await shInfoJury.saveUpdatedCells();
}

async function initJurySheets() {

  const jurySummaryDoc = await getDoc(theJurySummaryGSID);
  console.log(jurySummaryDoc.title);
  //console.log(jurySummaryDoc.sheetsByIndex);
  let shInfoSub = getSheetByTitle(jurySummaryDoc, "INFO-Submissions");
  let shInfoJury = getSheetByTitle(jurySummaryDoc, "JURY");

  const juryMemberTemplateDoc = await getDoc(theJuryTemplateGSID);
  console.log(juryMemberTemplateDoc.title);
  let shMemberTemplateInfoJury = getSheetByTitle(juryMemberTemplateDoc, "JURY-Template");

  // Loading cells locally
  await shInfoSub.loadCells(['A2:Q50']);
  await shInfoJury.loadCells(['A4:C50']);
  await shMemberTemplateInfoJury.loadCells(['A4:H50']);

  // Getting info from database
  let allSubmissions = await prisma.project.findMany();
  // Augmenting with topic information

  for (let proj of allSubmissions) {
      if (proj.id == "7f61e65d-8baf-49ef-b370-e835d48aedd2") continue;
      let challenge = await prisma.challenge.findOne({ where: { id: proj.challengeId } });
      let topic = await prisma.hackerTopic.findOne({where: {id: challenge.primaryTopicId}});
      proj["topicTitle"] = topic.title;
  }
  allSubmissions = allSubmissions.sort((a, b) => a["topicTitle"].localeCompare(b["topicTitle"]));
  //console.log(allSubmissions);
  //return;

  let submission_cnt = 0;
  // Looping over submissions in DB
  for (let proj of allSubmissions) {
      if (proj.id == "7f61e65d-8baf-49ef-b370-e835d48aedd2") continue;
      let challenge = await prisma.challenge.findOne({ where: { id: proj.challengeId } });
      let topic = await prisma.hackerTopic.findOne({where: {id: challenge.primaryTopicId}});

      const propertyToColMapping = [
          ++submission_cnt,
          "https://app.webfest.cern/submissions/" + proj.id,
          topic.title,
          challenge.title,
          "https://app.webfest.cern/challenges/" + challenge.id,
          proj.title,
          proj.tagline,
          proj.description,
          proj.technologiesUsed,
          proj.obstacles,
          proj.accomplishments,
          proj.learnings,
          proj.nextSteps,
          proj.relevanceToHackathon,
          proj.longTermImpact,
          proj.progressDuringHackathon,
          proj.valueAdded
      ];

      console.log(proj.id, proj.title);
      for (let c in propertyToColMapping) {
          let cell = shInfoSub.getCell(submission_cnt, c);
          cell.value = propertyToColMapping[c];
      }

      const juryInfoToColMapping = [
          proj.title,
          "https://app.webfest.cern/submissions/" + proj.id,
          topic.title
      ];

      for (let c in juryInfoToColMapping) {
        let cell = shInfoJury.getCell(2 + submission_cnt, c);
        cell.value = juryInfoToColMapping[c];
      }

      const juryMemberInfoToColMapping = [
        submission_cnt,
        proj.title,
        "https://app.webfest.cern/submissions/" + proj.id,
        topic.title,
        proj.relevanceToHackathon,
        proj.longTermImpact,
        proj.progressDuringHackathon,
        proj.valueAdded,
      ];

      for (let c in juryMemberInfoToColMapping) {
        let cell = shMemberTemplateInfoJury.getCell(2 + submission_cnt, c);
        cell.value = juryMemberInfoToColMapping[c];
      }

    }

    /*
    // WARNING: will set values to "" which is not BLANK
    // Clearing the remaining rows
    // Hard-coded
    for (let r = submission_cnt + 1; r < 50; r++) { 
        for (let c = 0 ; c < 17 ; c++) {
            let cell = shInfoSub.getCell(r, c);
            cell.value = "";
        }
    }

    for (let r = submission_cnt + 3; r < 35; r++) { 
        for (let c = 0 ; c < 3 ; c++) {
            let cell = shInfoJury.getCell(r, c);
            cell.value = "";
        }
    }

    for (let r = submission_cnt + 3; r < 35; r++) { 
        for (let c = 0 ; c < 8 ; c++) {
            let cell = shMemberTemplateInfoJury.getCell(r, c);
            cell.value = "";
        }
    }
    */

    // Updating sheets with local cache
    await shInfoSub.saveUpdatedCells();
    await shInfoJury.saveUpdatedCells();
    await shMemberTemplateInfoJury.saveUpdatedCells();
};

export default async () => {
  await initJurySheets();
  return;
  await setJuryMembers(theJurySummaryGSID);
};