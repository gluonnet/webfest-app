import { GoogleSpreadsheet } from "google-spreadsheet";

// use service account creds

//console.log(process.env.GOOGLE_SERVICE_ACCOUNT);

export const getBlankDoc = async () => {
  const doc = new GoogleSpreadsheet();
  await doc.useServiceAccountAuth(
    JSON.parse(process.env.GOOGLE_SERVICE_ACCOUNT)
  );
  await doc.loadInfo();
  return doc;
};

export const getDoc = async (sheetId: string) => {
  const doc = new GoogleSpreadsheet(sheetId);

  await doc.useServiceAccountAuth(
    JSON.parse(process.env.GOOGLE_SERVICE_ACCOUNT)
  );
  await doc.loadInfo();
  return doc;
};
