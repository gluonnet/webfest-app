import schedule from "./schedule";
import challenges from "./challenges";
import users from "./users";
import users_from_indico from "./users_from_indico";
import mentors from "./mentors";
import hackerSkills from "./hackerSkills";
import hackerTopics from "./hackerTopic";
import hackerTypes from "./hackerType";
import teams from "./teams";
import makeSheetForJury from "./makeSheetForJury";
import cleanup from "./cleanup";
import build_teams_from_spreadsheet from "./build_teams_from_spreadsheet";

export const seed = async () => {
//  await cleanup();
//  await users_from_indico();
  await build_teams_from_spreadsheet();
//  await teams();
//  await makeSheetForJury();
//  await users();
//  await schedule();
//  await hackerSkills();
//  await hackerTypes();
//  await hackerTopics();
//  await mentors();
//  await challenges();
};
