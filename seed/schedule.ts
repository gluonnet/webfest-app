import prisma from "../prisma/prisma";
import { ScheduleCreateInput, ScheduleType } from "@prisma/client";
import moment from "moment";

const SCHEDULES_TO_SEED: ScheduleCreateInput[] = [
  {
    title: "Sign in IT support",
    from: "2020-04-03T08:00:00.000+02:00",
    to: "2020-04-03T10:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: "https://versusvirus.slack.com/archives/C010JA67ZQU"
  },
  {
    title: "Teambuilding Session",
    from: "2020-04-03T10:00:00.000+02:00",
    to: "2020-04-03T12:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: "https://versusvirus.slack.com/archives/C0109BWA3QA"
  },
  {
    title: "Icebreaker",
    from: "2020-04-03T10:00:00.000+02:00",
    to: "2020-04-03T11:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: "https://www.youtube.com/watch?v=SZMg5JyL0fc"
  },
  {
    title: "Problem framing",
    from: "2020-04-03T11:00:00.000+02:00",
    to: "2020-04-03T12:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: "https://versusvirus.slack.com/archives/C0109BWA3QA"
  },
  {
    title: "Lunch break",
    from: "2020-04-03T12:00:00.000+02:00",
    to: "2020-04-03T15:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: "https://www.youtube.com/watch?v=SZMg5JyL0fc"
  },
  {
    title: "Arrival check in",
    from: "2020-04-03T15:00:00.000+02:00",
    to: "2020-04-03T15:30:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: "https://versusvirus.slack.com/archives/C010JA67ZQU"
  },
  {
    title: "Challenge selection",
    from: "2020-04-03T15:00:00.000+02:00",
    to: "2020-04-03T15:30:00.000+02:00",
    type: ScheduleType.INTERNAL_LINK,
    data: "/challengeselection"
  },
  {
    title: "Kick-off event",
    from: "2020-04-03T15:30:00.000+02:00",
    to: "2020-04-03T16:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: "https://www.youtube.com/watch?v=SZMg5JyL0fc"
  },
  {
    title: "Hacking begins",
    from: "2020-04-03T15:30:00.000+02:00",
    to: "2020-04-03T24:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: 'Input session: "How to frame the problem"',
    from: "2020-04-03T16:00:00.000+02:00",
    to: "2020-04-03T16:30:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: "Alain Berset speech",
    from: "2020-04-03T17:30:00.000+02:00",
    to: "2020-04-03T18:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: 'Input session:"How to create ideas"',
    from: "2020-04-03T18:30:00.000+02:00",
    to: "2020-04-03T19:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: "https://zoom.us/j/962751985"
  },
  {
    title: "Energizer",
    from: "2020-04-03T20:00:00.000+02:00",
    to: "2020-04-03T24:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: "Energizer",
    from: "2020-04-04T08:00:00.000+02:00",
    to: "2020-04-04T10:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: "E-Meet the Mentors",
    from: "2020-04-04T09:00:00.000+02:00",
    to: "2020-04-04T20:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: "E-Meet the Challenge Experts",
    from: "2020-04-04T09:00:00.000+02:00",
    to: "2020-04-04T20:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: "IT support",
    from: "2020-04-04T09:00:00.000+02:00",
    to: "2020-04-04T20:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: 'Input session: "How to create a concept"',
    from: "2020-04-04T10:00:00.000+02:00",
    to: "2020-04-04T10:30:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: 'Input session: "How to rapid prototype"',
    from: "2020-04-04T13:00:00.000+02:00",
    to: "2020-04-04T13:30:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: "Update session",
    from: "2020-04-04T14:00:00.000+02:00",
    to: "2020-04-04T14:30:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: 'Input session: "How to pitch online"',
    from: "2020-04-04T18:00:00.000+02:00",
    to: "2020-04-04T18:30:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: "Update session + Final presentation",
    from: "2020-04-04T19:00:00.000+02:00",
    to: "2020-04-04T19:30:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: "Energizer",
    from: "2020-04-04T20:00:00.000+02:00",
    to: "2020-04-04T24:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: "Energizer",
    from: "2020-04-05T08:00:00.000+02:00",
    to: "2020-04-05T10:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: 'Input session: "How to pitch online"',
    from: "2020-04-05T10:00:00.000+02:00",
    to: "2020-04-05T10:30:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: "Hand in presentation",
    from: "2020-04-05T17:30:00.000+02:00",
    to: "2020-04-05T18:00:00.000+02:00",
    type: ScheduleType.INTERNAL_LINK,
    data: "/project#results"
  },
  {
    title: "Final words, acknowledgements, next steps",
    from: "2020-04-05T18:00:00.000+02:00",
    to: "2020-04-05T18:30:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  },
  {
    title: "Celebration",
    from: "2020-04-05T18:30:00.000+02:00",
    to: "2020-04-05T22:00:00.000+02:00",
    type: ScheduleType.EXTERNAL_LINK,
    data: ""
  }
].map(e => ({
  ...e,
  from: moment(e.from).toDate(),
  to: moment(e.to).toDate()
}));

export default async () => {
  // curretly disabled
  return;
  for (let schedule of SCHEDULES_TO_SEED) {
    await prisma.schedule.upsert({
      where: {
        from_to: {
          from: schedule.from,
          to: schedule.to
        }
      },
      create: schedule,
      update: {}
    });
  }
};
