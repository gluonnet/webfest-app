import omit from 'lodash/omit'
import prisma from "../prisma/prisma";

const Users = [
  {
    email: "karolos@gluonnet.org",
    firstname: "Karolos",
    lastname: "Potamianos",
    emailConfirmed: true,
    phoneNumber: "0123456789",
    city: "Geneva",
    roles: { connect: [ { id: "admin" }, { id: "organiser" } ] },
  },
  {
    email: "daniel@gluonnet.org",
    firstname: "Daniel",
    lastname: "Dobos",
    emailConfirmed: true,
    phoneNumber: "0123456789",
    city: "Geneva",
    roles: { connect: [ { id: "admin" }, { id: "organiser" } ] },
  },
  {
    email: "alexia.marie.yiannouli@cern.ch",
    firstname: "Alexia",
    lastname: "Yiannouli",
    emailConfirmed: true,
    phoneNumber: "0123456789",
    city: "Geneva",
    roles: { connect: [ { id: "admin" }, { id: "organiser" } ] },
  },
  {
    email: "anastasiia.lazuka@cern.ch",
    firstname: "Anastasiia",
    lastname: "Lazuka",
    emailConfirmed: true,
    phoneNumber: "0123456789",
    city: "Geneva",
    roles: { connect: [ { id: "admin" }, { id: "organiser" } ] },
  },
  {
    email: "kristiane.novotny@cern.ch",
    firstname: "Kristiane",
    lastname: "Bernhard",
    emailConfirmed: true,
    phoneNumber: "0123456789",
    city: "Geneva",
    roles: { connect: [ { id: "admin" }, { id: "organiser" } ] },
  },
];

export default async () => {

  const deleteSchedules = await prisma.schedule.deleteMany({});
  const deleteTeams = await prisma.team.deleteMany({});
  const deleteProjecvts = await prisma.project.deleteMany({});
  const deleteChallenges = await prisma.challenge.deleteMany({});
  const deleteHackerTopics = await prisma.hackerTopic.deleteMany({
    where: {
      NOT: {
        id: "climateaction",
      },
    }
  });
  const deleteVotes = await prisma.teamChallengeVote.deleteMany({});
  const deleteUsers = await prisma.user.deleteMany({
    where: {
      NOT: {
        email: {
	  in: [ "karolos@gluonnet.org" ],
        },
      }
    }
  });

  for (let user of Users) {
    await prisma.user.upsert({
      where: { email: user.email },
      create: user,
      update: {},
    });
  }
};
